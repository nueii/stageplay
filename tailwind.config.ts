import {
	radixAnimations,
	tailwindAnimate,
	tailwindExtensions,
} from "@itsmapleleaf/configs/tailwind"
import type { Config } from "tailwindcss"
import colors from "tailwindcss/colors"

export default {
	presets: [tailwindExtensions],
	content: ["./app/**/*.{js,jsx,ts,tsx}"],
	theme: {
		extend: {
			fontFamily: {
				sans: `"Sofia Sans Variable", sans-serif`,
			},
			colors: {
				shade: {
					900: "hsl(276, 14%, 7%)",
					800: "hsl(278, 12%, 13%)",
					700: "hsl(276, 12%, 16%)",
				},
				accent: colors.purple,
			},
			screens: {
				xs: "480px",
			},
			animation: {},
			keyframes: {},
		},
	},
	plugins: [
		tailwindAnimate(),
		radixAnimations({
			fade: {
				enter: {
					opacity: "1",
				},
				exit: {
					opacity: "0",
				},
			},
			"fade-slide-down": {
				enter: {
					opacity: "1",
					transform: "translateY(0)",
				},
				exit: {
					opacity: "0",
					transform: "translateY(1rem)",
				},
			},
			"fade-zoom": {
				enter: {
					opacity: "1",
					transform: "scale(1)",
				},
				exit: {
					opacity: "0",
					transform: "scale(0.7)",
				},
			},
		}),
	],
} satisfies Config
