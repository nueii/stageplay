# syntax = docker/dockerfile:1

# Adjust NODE_VERSION as desired
ARG NODE_VERSION=lts
FROM node:${NODE_VERSION}-slim as base

LABEL fly_launch_runtime="Node.js/Prisma"

# Install system dependencies
RUN apt-get update -y && apt-get install -y openssl

# Enable corepack
RUN corepack enable

# Node.js/Prisma app lives here
WORKDIR /app

# Install deps
COPY --link package.json pnpm-lock.yaml ./
RUN corepack install
RUN pnpm install --frozen-lockfile

# Generate Prisma Client
COPY --link prisma ./prisma
RUN pnpm prisma generate

# Build application
COPY --link . .
RUN pnpm run build

# Set production environment
ENV NODE_ENV="production"

# Prune dev dependencies
RUN pnpm prune --prod

# Prune the pnpm store
RUN pnpm store prune

# Expose web server port
EXPOSE 3000
