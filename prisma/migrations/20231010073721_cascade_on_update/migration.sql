-- DropForeignKey
ALTER TABLE "ActorChannelConfig" DROP CONSTRAINT "ActorChannelConfig_actorId_fkey";

-- DropForeignKey
ALTER TABLE "ActorChannelConfig" DROP CONSTRAINT "ActorChannelConfig_characterId_fkey";

-- AddForeignKey
ALTER TABLE "ActorChannelConfig" ADD CONSTRAINT "ActorChannelConfig_actorId_fkey" FOREIGN KEY ("actorId") REFERENCES "Actor"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ActorChannelConfig" ADD CONSTRAINT "ActorChannelConfig_characterId_fkey" FOREIGN KEY ("characterId") REFERENCES "Character"("id") ON DELETE CASCADE ON UPDATE CASCADE;
