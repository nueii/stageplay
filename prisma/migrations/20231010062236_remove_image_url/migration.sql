-- for each character, add the imageUrl to the start of the images array if it exists, is not an empty string, and doesn't already exist in the array
UPDATE "Character" SET "images" = array_prepend("imageUrl", "images") WHERE "imageUrl" != '' AND "imageUrl" IS NOT NULL AND NOT "imageUrl" = ANY("images");

-- AlterTable
ALTER TABLE "Character" DROP COLUMN "imageUrl";
