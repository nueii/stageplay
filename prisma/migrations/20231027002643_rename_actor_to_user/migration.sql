-- DropForeignKey
ALTER TABLE "ActorChannelConfig" DROP CONSTRAINT "ActorChannelConfig_actorId_fkey";

-- DropForeignKey
ALTER TABLE "Character" DROP CONSTRAINT "Character_actorId_fkey";

-- DropForeignKey
ALTER TABLE "Session" DROP CONSTRAINT "Session_actorId_fkey";

-- DropIndex
DROP INDEX "ActorChannelConfig_actorId_channelId_key";

-- AlterTable
ALTER TABLE "ActorChannelConfig" RENAME COLUMN "actorId" TO "userId";

-- AlterTable
ALTER TABLE "Character" RENAME COLUMN "actorId" TO "userId";

-- AlterTable
ALTER TABLE "Session" RENAME COLUMN "actorId" TO "userId";

-- AlterTable
ALTER TABLE "Actor" RENAME TO "User";

-- CreateIndex
CREATE UNIQUE INDEX "User_discordId_key" ON "User"("discordId");

-- CreateIndex
CREATE UNIQUE INDEX "ActorChannelConfig_userId_channelId_key" ON "ActorChannelConfig"("userId", "channelId");

-- AddForeignKey
ALTER TABLE "Session" ADD CONSTRAINT "Session_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Character" ADD CONSTRAINT "Character_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ActorChannelConfig" ADD CONSTRAINT "ActorChannelConfig_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
