import chalk from "chalk"
import { oraPromise } from "ora"
import prettyMilliseconds from "pretty-ms"

export const logger = {
	info: (...args: unknown[]) => {
		console.info(chalk.blue`[info]`, ...args)
	},
	warn: (...args: unknown[]) => {
		console.warn(chalk.yellow`[warn]`, ...args)
	},
	error: (...args: unknown[]) => {
		console.error(chalk.red`[oops]`, ...args)
	},
	promise: <T>(message: string, promise: Promise<T>) => {
		const startTime = Date.now()
		return oraPromise(promise, {
			text: message,
			successText: () => {
				const runtime = prettyMilliseconds(Date.now() - startTime)
				return `${message} ${chalk.gray(`(${runtime})`)}`
			},
		})
	},
}
