import * as z from "zod"

const envSchema = z.object({
	NODE_ENV: z
		.enum(["development", "production", "test"])
		.default("development"),
	DISCORD_BOT_TOKEN: z.string(),
	FRONTEND_URL: z.string().url().optional(),
})

export const env = envSchema.parse(process.env)
