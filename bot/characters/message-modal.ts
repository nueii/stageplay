import { TextInputStyle } from "discord.js"
import { Modal } from "../discord/modal"

export const createMessageModal = (customId: string) =>
	new Modal(customId, {
		message: {
			label: "Message",
			style: TextInputStyle.Paragraph,
			placeholder: "New message text",
		},
	})
