import type { Character, User } from "@prisma/client"
import type { RepliableInteraction } from "discord.js"
import {
	ActionRowBuilder,
	ButtonBuilder,
	ButtonStyle,
	EmbedBuilder,
} from "discord.js"
import { addEphemeralInteractionReply } from "../discord/add-interaction-response"
import { getFrontendUrl } from "../frontend"
import { isUrl } from "../helpers/is-url"

export async function showCharacterProfile(
	interaction: RepliableInteraction,
	character: Character & { user: User },
	messageContent?: string,
) {
	const isOwner = interaction.user.id === character.user.discordId

	const embed = new EmbedBuilder()
		.setAuthor({
			name: character.name,
			iconURL: isUrl(character.avatarUrl) ? character.avatarUrl : undefined,
		})
		.addFields(
			{
				name: "Created by",
				value: `<@${character.user.discordId}>`,
				inline: true,
			},
			{
				name: "Created",
				value: `<t:${Math.floor(character.createdAt.getTime() / 1000)}:R>`,
				inline: true,
			},
			{
				name: "Updated",
				value: `<t:${Math.floor(character.updatedAt.getTime() / 1000)}:R>`,
				inline: true,
			},
		)

	if (interaction.user.id === character.user.discordId) {
		embed.addFields({
			name: "Prefix",
			value: character.prefix || "(no prefix set)",
			inline: true,
		})
	}

	if (character.bio) {
		embed.setDescription(character.bio)
	}

	const imageUrl = character.images.find(isUrl)
	if (imageUrl) {
		embed.setImage(imageUrl)
	}

	const row = new ActionRowBuilder<ButtonBuilder>().addComponents(
		new ButtonBuilder()
			.setLabel("View Profile")
			.setEmoji("📖")
			.setStyle(ButtonStyle.Link)
			.setURL(getFrontendUrl(`/characters/${character.id}`)),
	)

	if (isOwner) {
		row.addComponents(
			new ButtonBuilder()
				.setLabel("Edit")
				.setEmoji("✏️")
				.setStyle(ButtonStyle.Link)
				.setURL(getFrontendUrl(`/characters/${character.id}/edit`)),
			new ButtonBuilder()
				.setLabel("Delete")
				.setEmoji("🗑️")
				.setStyle(ButtonStyle.Link)
				.setURL(getFrontendUrl(`/characters/${character.id}/delete`)),
		)
	}

	await addEphemeralInteractionReply(interaction, {
		content: messageContent,
		embeds: [embed],
		components: [row],
	})
}
