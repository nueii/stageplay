import { WebhookClient } from "discord.js"
import { defineMessageCommand } from "../command-framework/message-command"
import { getChannelThreadId } from "./get-channel-thread-id"
import { createMessageModal } from "./message-modal"
import { requireCharacterWebhook } from "./require-character-webhook"
import { requireOwnedCharacter } from "./require-owned-character"

export const editMessageCommand = defineMessageCommand({
	name: "Edit Message",
	run: async (interaction) => {
		const webhook = await requireCharacterWebhook(interaction.targetMessage)

		requireOwnedCharacter(
			webhook.character,
			interaction.user.id,
			"You can only edit your own characters' messages!",
		)

		const messageModal = createMessageModal(
			`edit-message:${interaction.targetMessage.id}`,
		)

		await interaction.showModal(
			messageModal.data("Edit message", {
				message: interaction.targetMessage.content,
			}),
		)

		const submission = await messageModal.submission(interaction)
		if (!submission) return

		await submission.deferUpdate()

		const data = messageModal.getResponseData(submission)

		const client = new WebhookClient({ url: webhook.webhookUrl })
		await client.editMessage(interaction.targetMessage.id, {
			content: data.message,
			threadId: getChannelThreadId(interaction.targetMessage.channel),
		})
	},
})
