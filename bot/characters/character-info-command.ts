import { defineMessageCommand } from "../command-framework/message-command"
import { requireCharacterWebhook } from "./require-character-webhook"
import { showCharacterProfile } from "./show-character-profile"

export const characterInfoCommand = defineMessageCommand({
	name: "Character Info",
	run: async (interaction) => {
		const { character } = await requireCharacterWebhook(
			interaction.targetMessage,
		)
		await showCharacterProfile(interaction, character)
	},
})
