import type { Channel } from "discord.js"

export function getChannelThreadId(channel: Channel) {
	return channel.isThread() ? channel.id : undefined
}
