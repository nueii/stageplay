import { WebhookClient } from "discord.js"
import { defineMessageCommand } from "../command-framework/message-command"
import { getChannelThreadId } from "./get-channel-thread-id"
import { requireCharacterWebhook } from "./require-character-webhook"
import { requireOwnedCharacter } from "./require-owned-character"

export const deleteMessageCommand = defineMessageCommand({
	name: "Delete Message",
	run: async (interaction) => {
		const webhook = await requireCharacterWebhook(interaction.targetMessage)

		requireOwnedCharacter(
			webhook.character,
			interaction.user.id,
			"You can only delete your own characters' messages!",
		)

		await interaction.deferReply()

		const client = new WebhookClient({ url: webhook.webhookUrl })
		await Promise.all([
			client.deleteMessage(
				interaction.targetMessage.id,
				getChannelThreadId(interaction.targetMessage.channel),
			),
			interaction.deleteReply(),
		])
	},
})
