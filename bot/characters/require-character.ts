import { db } from "../core/db"
import { InteractionError } from "../discord/interaction-error"
import { raise } from "../helpers/raise"

export async function requireCharacter(id: string) {
	const character = await db.character.findUnique({
		where: { id },
		include: { user: true },
	})
	return (
		character ??
		raise(new InteractionError("Oops, couldn't find who you were looking for."))
	)
}
