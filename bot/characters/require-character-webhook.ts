import type { Character, CharacterWebhook, User } from "@prisma/client"
import type { Message } from "discord.js"
import { db } from "../core/db"
import { InteractionError } from "../discord/interaction-error"
import { requireMessageWebhook } from "../discord/require-message-webhook"
import { raise } from "../helpers/raise"

export async function requireCharacterWebhook(
	message: Message,
): Promise<CharacterWebhook & { character: Character & { user: User } }> {
	const webhookId = requireMessageWebhook(message)

	const webhook = await db.characterWebhook.findUnique({
		where: { webhookId },
		include: {
			character: {
				include: { user: true },
			},
		},
	})

	return (
		webhook ??
		raise(
			new InteractionError(
				`Couldn't find that character!\nTry \`/characters view ${message.author.username}\``,
			),
		)
	)
}
