import type { AutocompleteInteraction } from "discord.js"
import { db } from "../core/db"

export async function autocompleteSelfCharacter(
	value: string,
	interaction: AutocompleteInteraction<"cached">,
) {
	const characters = await db.character.findMany({
		where: {
			AND: {
				name: { contains: value, mode: "insensitive" },
				user: { discordId: interaction.user.id },
			},
		},
		orderBy: [
			{ lastPlayedAt: { sort: "desc", nulls: "last" } },
			{ updatedAt: "desc" },
		],
		select: { id: true, name: true },
	})

	return characters.map((character) => ({
		name: character.name,
		value: character.id,
	}))
}
