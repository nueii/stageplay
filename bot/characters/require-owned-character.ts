import { InteractionError } from "../discord/interaction-error"
import { raise } from "../helpers/raise"

export function requireOwnedCharacter<
	T extends { user: { discordId: string } },
>(
	character: T,
	userId: string,
	errorMessage = "You don't own this character",
): T {
	return character.user.discordId === userId
		? character
		: raise(new InteractionError(errorMessage))
}
