import { defineGroupCommand } from "../command-framework/group-command"
import { defineSlashCommand } from "../command-framework/slash-command"
import { getFrontendUrl } from "../frontend"
import { autocompleteSelfCharacter } from "./character-options"
import { requireCharacter } from "./require-character"
import { requireOwnedCharacter } from "./require-owned-character"
import { showCharacterProfile } from "./show-character-profile"

const createCommand = defineSlashCommand((context) => {
	const getName = context.stringOption({
		name: "name",
		description: "The name of the character",
	})

	return {
		name: "create",
		description: "Create a new character",
		run: async (interaction) => {
			const name = getName()
			let url = getFrontendUrl("/characters/new")
			if (name) {
				url += `?name=${encodeURIComponent(name)}`
			}
			return interaction.reply({
				ephemeral: true,
				content: `Finish creating your character on the website: ${url}`,
			})
		},
	}
})

const editCommand = defineSlashCommand((context) => {
	const getCharacter = context.stringOption.required({
		name: "character",
		description: "The character",
		autocomplete: autocompleteSelfCharacter,
	})

	return {
		name: "edit",
		description: "Edit an existing character",
		run: async (interaction) => {
			const character = requireOwnedCharacter(
				await requireCharacter(getCharacter()),
				interaction.user.id,
				"You can only edit your own characters!",
			)

			const url = getFrontendUrl(`/characters/${character.id}/edit`)
			return interaction.reply({
				ephemeral: true,
				content: `Edit your character on the website: ${url}`,
			})
		},
	}
})

const viewCommand = defineSlashCommand((context) => {
	const getCharacter = context.stringOption.required({
		name: "character",
		description: "The character",
		autocomplete: autocompleteSelfCharacter,
	})

	return {
		name: "view",
		description: "View a character profile",
		run: async (interaction) => {
			const character = await requireCharacter(getCharacter())
			await showCharacterProfile(interaction, character)
		},
	}
})

const deleteCommand = defineSlashCommand((context) => {
	const getCharacter = context.stringOption.required({
		name: "character",
		description: "The character",
		autocomplete: autocompleteSelfCharacter,
	})

	return {
		name: "delete",
		description: "Delete a character",
		run: async (interaction) => {
			const character = requireOwnedCharacter(
				await requireCharacter(getCharacter()),
				interaction.user.id,
				"You can only edit your own characters!",
			)

			const url = getFrontendUrl(`/characters/${character.id}/delete`)
			return interaction.reply({
				ephemeral: true,
				content: `Continue on the website: ${url}`,
			})
		},
	}
})

const importCommand = defineSlashCommand((context) => {
	const getCharacter = context.stringOption.required({
		name: "character",
		description: "The name or URL of the character",
	})

	return {
		name: "import",
		description: "Import a character from F-list",
		run: async (interaction) => {
			const characterParam = encodeURIComponent(getCharacter())
			const url = getFrontendUrl(
				`/characters/import?character=${characterParam}`,
			)
			return interaction.reply({
				ephemeral: true,
				content: `Continue on the website: ${url}`,
			})
		},
	}
})

export const characterSuperCommand = defineGroupCommand(
	"characters",
	"Manage characters",
	[createCommand, viewCommand, editCommand, deleteCommand, importCommand],
)
