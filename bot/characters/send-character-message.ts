import type { Character } from "@prisma/client"
import console from "console"
import type { Channel, Guild, User } from "discord.js"
import { WebhookClient } from "discord.js"
import { db } from "../core/db"
import { logger } from "../core/logger"

export async function sendCharacterMessage(
	character: Character,
	messageText: string,
	guild: Guild,
	channel: Channel,
	author: User,
) {
	const thread = channel.isThread() ? channel : undefined
	const webhookChannelId = thread?.parentId ?? channel.id

	const characterWebhook = await db.characterWebhook.findFirst({
		where: {
			characterId: character.id,
			channelId: webhookChannelId,
		},
	})

	let success = false

	if (characterWebhook) {
		try {
			const url = new URL(characterWebhook.webhookUrl)
			if (thread) {
				url.searchParams.set("thread_id", thread.id)
			}

			const res = await fetch(url.href, {
				method: "POST",
				headers: {
					"Content-Type": "application/x-www-form-urlencoded",
				},
				body: new URLSearchParams({
					avatar_url: character.avatarUrl,
					content: messageText,
				}),
			})

			if (res.ok) {
				success = true
			} else {
				throw new Error(`Webhook returned ${res.status}: ${await res.text()}`)
			}
		} catch (error) {
			console.warn("Failed to fetch webhook", {
				character,
				webhook: characterWebhook,
				error,
			})
		}
	}

	if (!success) {
		try {
			const guildWebhooks = await guild.fetchWebhooks()

			// we can't have more than ten webhooks per channel; delete the oldest one if we have to
			const webhooksInChannel = guildWebhooks?.filter(
				(w) => w.channelId === webhookChannelId,
			)
			if (webhooksInChannel.size >= 10) {
				await webhooksInChannel
					.sorted((a, b) => a.createdTimestamp - b.createdTimestamp)
					.first()
					?.delete()
			}

			// workaround: `fetch` in Discord.js fails to resolve avatar images,
			// so do it ourselves
			const avatarResponse = await fetch(character.avatarUrl)
			const avatarBuffer = Buffer.from(await avatarResponse.arrayBuffer())
			const webhook = await guild.channels.createWebhook({
				name: character.name,
				channel: webhookChannelId,
				avatar: avatarBuffer,
				reason: `Automatically created webhook for ${character.name}`,
			})

			const data = {
				webhookUrl: webhook.url,
				channelId: webhook.channelId,
				characterId: character.id,
			}

			await db.characterWebhook.upsert({
				where: { webhookId: webhook.id },
				create: { ...data, webhookId: webhook.id },
				update: data,
			})

			await new WebhookClient(webhook).send({
				avatarURL: character.avatarUrl,
				content: messageText,
				threadId: thread?.id,
			})
		} catch (error) {
			console.error(error)

			await author.send({
				content:
					"Oops! Something went wrong while sending your message. It might be too long. I saved the text for you!",
				files: [
					{
						name: "message.txt",
						attachment: Buffer.from(messageText),
						contentType: "text/plain; charset=utf-8",
					},
				],
			})
			return
		}
	}

	// nonessential update
	await db.character
		.update({
			where: { id: character.id },
			data: { lastPlayedAt: new Date() },
		})
		.catch(logger.error)
}
