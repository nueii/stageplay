import type { Message } from "discord.js"
import { db } from "../core/db"

export async function getCharacterAndMessageFromPrefix(message: Message<true>) {
	const characters = await db.character.findMany({
		where: {
			user: { discordId: message.author.id },
		},
	})

	const fullMessage = message.content.trim()

	const character = characters
		.flatMap((character) => {
			if (!character.prefix) return []
			return { ...character, prefix: character.prefix.trim() }
		})
		.find((c) => fullMessage.toLowerCase().startsWith(c.prefix.toLowerCase()))

	if (!character) return

	const characterMessage = fullMessage.slice(character.prefix.length).trim()
	if (characterMessage.length === 0) return

	return { character, message: characterMessage }
}

export async function getCurrentCharacterFromMessage(message: Message<true>) {
	const config = await db.actorChannelConfig.findFirst({
		where: {
			user: { discordId: message.author.id },
			channelId: message.channel.id,
		},
		select: {
			character: true,
		},
	})
	return config?.character
}
