import { defineSlashCommand } from "../command-framework/slash-command"
import { InteractionError } from "../discord/interaction-error"
import { requireInteractionGuild } from "../discord/require-interaction-guild"
import { autocompleteSelfCharacter } from "./character-options"
import { createMessageModal } from "./message-modal"
import { requireCharacter } from "./require-character"
import { requireOwnedCharacter } from "./require-owned-character"
import { sendCharacterMessage } from "./send-character-message"

export const playCommand = defineSlashCommand((context) => {
	const getCharacter = context.stringOption.required({
		name: "character",
		description: "The character",
		autocomplete: autocompleteSelfCharacter,
	})

	const getMessage = context.stringOption({
		name: "message",
		description: "Quickly send a message (no line breaks)",
	})

	return {
		name: "play",
		description: "Send a message as a character",
		run: async (interaction) => {
			const guild = requireInteractionGuild(interaction)

			if (!interaction.channel) {
				throw new InteractionError("This command can only be used in a channel")
			}

			const channelId = interaction.channel.isThread()
				? interaction.channel.parentId
				: interaction.channel.id

			if (!channelId) {
				throw new InteractionError(
					"Huh, that's weird... does this thread exist? 🤔",
				)
			}

			const character = requireOwnedCharacter(
				await requireCharacter(getCharacter()),
				interaction.user.id,
				"You can only play your own characters, silly!",
			)

			const messageModal = createMessageModal(
				`play:${character.id}:${channelId}`,
			)

			let messageText = getMessage()?.trim()
			if (messageText) {
				const deferPromise = interaction.deferReply({ ephemeral: true })

				await sendCharacterMessage(
					character,
					messageText,
					guild,
					interaction.channel,
					interaction.user,
				)

				await deferPromise
				await interaction.deleteReply()
			} else {
				await interaction.showModal(
					messageModal.data(`Send a message as ${character.name}`),
				)

				const submission = await messageModal.submission(interaction)
				if (!submission) return

				await submission.deferUpdate()

				messageText = messageModal.getResponseData(submission).message

				await sendCharacterMessage(
					character,
					messageText,
					guild,
					interaction.channel,
					interaction.user,
				)
			}
		},
	}
})
