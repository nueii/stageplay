import { defineSlashCommand } from "../command-framework/slash-command"
import { db } from "../core/db"
import { getFrontendUrl } from "../frontend"
import { autocompleteSelfCharacter } from "./character-options"
import { requireCharacter } from "./require-character"
import { requireOwnedCharacter } from "./require-owned-character"

export const setCommand = defineSlashCommand((context) => {
	const getCharacter = context.stringOption.required({
		name: "character",
		description: "The character",
		autocomplete: autocompleteSelfCharacter,
	})

	return {
		name: "set",
		description: "Set the current character for this channel",
		run: async (interaction) => {
			const character = requireOwnedCharacter(
				await requireCharacter(getCharacter()),
				interaction.user.id,
				"Sorry, you can only play as your own characters!",
			)

			await updateChannelConfig({
				userId: character.userId,
				channelId: interaction.channelId,
				characterId: character.id,
			})

			const characterProfileUrl = getFrontendUrl(`/characters/${character.id}`)
			return interaction.reply({
				ephemeral: true,
				content: `Done! Everything you say in this channel will be sent as **[${character.name}](${characterProfileUrl})**. Run \`/unset\` to stop.`,
			})
		},
	}
})

export const unsetCommand = defineSlashCommand(() => ({
	name: "unset",
	description: "Unset the current character for this channel",
	run: async (interaction) => {
		const user = await upsertUserFromDiscordUser(interaction.user)

		await updateChannelConfig({
			userId: user.id,
			channelId: interaction.channelId,
			characterId: null,
		})

		return interaction.reply({
			ephemeral: true,
			content: "Done! Everything you say in this channel will be sent as you.",
		})
	},
}))

export const whoamiCommand = defineSlashCommand(() => ({
	name: "whoami",
	description: "Which character is this channel set to?",
	run: async (interaction) => {
		const config = await db.actorChannelConfig.findFirst({
			where: {
				user: { discordId: interaction.user.id },
				channelId: interaction.channelId,
			},
			select: {
				character: {
					select: {
						name: true,
					},
				},
			},
		})

		return interaction.reply({
			content: config?.character
				? `You are playing as **${config.character.name}** in this channel. Use \`/set\` to play as someone else!`
				: "This channel has no character set to it. Use `/set` to play as someone!",
			ephemeral: true,
		})
	},
}))

function upsertUserFromDiscordUser(discordUser: {
	id: string
	username: string
	avatarURL: () => string | null
}) {
	return db.user.upsert({
		where: {
			discordId: discordUser.id,
		},
		create: {
			discordId: discordUser.id,
			name: discordUser.username,
			avatarUrl: discordUser.avatarURL(),
		},
		update: {
			discordId: discordUser.id,
			name: discordUser.username,
			avatarUrl: discordUser.avatarURL(),
		},
	})
}

function updateChannelConfig({
	userId,
	channelId,
	...config
}: {
	userId: string
	channelId: string
	characterId: string | null
}) {
	return db.actorChannelConfig.upsert({
		where: {
			userId_channelId: {
				userId,
				channelId,
			},
		},
		create: {
			...config,
			userId,
			channelId,
		},
		update: config,
	})
}
