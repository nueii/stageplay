/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-unsafe-call */

/* eslint-disable @typescript-eslint/no-unsafe-assignment */

// @ts-nocheck
import { required, stringOption } from "../discord/command-option"
import { SlashCommand } from "../discord/slash-command"

export const rollCommand = new SlashCommand({
	name: "roll",
	description: "Roll some dice!",
	options: {
		dice: required(stringOption("Dice string, e.g. 2d6+1")),
	},
	run(interaction, options) {
		const dice = options.dice
		const result = roll(dice)
		return interaction.reply(result)
	},
})

type Token =
	| { type: "dice"; count: number; sides: number; sign: 1 | -1 }
	| { type: "constant"; value: number }

function parseRollString(input: string) {
	input = input.replace(/\s/g, "")

	const tokens: Token[] = []
	let position = 0

	// eslint-disable-next-line prefer-const
	const sign = 1
	while (position < input.length) {
		const [constantMatch] = input.slice(position).match(/^\d+/) ?? []
		if (constantMatch) {
			tokens.push({ type: "constant", value: sign * Number(constantMatch) })
			position += constantMatch.length
			continue
		}
	}
}
