import type {
	ApplicationCommandData,
	AutocompleteInteraction,
	CommandInteraction,
} from "discord.js"

export type Command = {
	readonly data: ApplicationCommandData
	getAction(
		interaction:
			| CommandInteraction<"cached">
			| AutocompleteInteraction<"cached">,
	): CommandAction | undefined
}

export type CommandAction = () => Promise<void>

export async function runMatchingCommand(
	interaction: CommandInteraction<"cached"> | AutocompleteInteraction<"cached">,
	commands: Command[],
) {
	for (const command of commands) {
		const action = command.getAction(interaction)
		if (action) return action()
	}
}
