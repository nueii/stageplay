import * as Discord from "discord.js"
import type { MaybePromise, Simplify, StrictOmit } from "../helpers/types"
import type { Command } from "./command"

export type SlashCommandSetupResult =
	Discord.ChatInputApplicationCommandData & {
		run: (interaction: Discord.ChatInputCommandInteraction<"cached">) => unknown
	}

export type SlashCommandOptionData = NonNullable<
	Discord.ApplicationCommandSubCommandData["options"]
>[number]

export interface SlashCommand extends Command {
	getAction(
		interaction:
			| Discord.ChatInputCommandInteraction<"cached">
			| Discord.AutocompleteInteraction<"cached">,
		name?: string,
	): (() => Promise<void>) | undefined
	data: StrictOmit<Discord.ChatInputApplicationCommandData, "options"> & {
		options: readonly SlashCommandOptionData[]
	}
}

export function defineSlashCommand(
	setup: (context: SlashCommandSetupContext) => SlashCommandSetupResult,
): SlashCommand {
	const state = new SetupState()
	const { run, ...data } = setup(createSetupContext(state))
	return {
		data: {
			...data,
			options: state.options,
		},
		getAction(interaction, commandName = interaction.commandName) {
			if (commandName !== data.name) {
				return
			}

			if (interaction.isChatInputCommand()) {
				return async () => {
					state.currentInteraction = interaction
					await run(interaction)
				}
			}

			if (!interaction.isAutocomplete()) {
				return
			}

			const focused = interaction.options.getFocused(true)
			const completion = state.autocompletions.find(
				(x) => x.name === focused.name,
			)
			if (!completion) return

			return async () => {
				const choices = await completion.getChoices(focused.value, interaction)
				await interaction.respond(
					choices
						.map((value) =>
							typeof value === "string" ? { name: value, value } : value,
						)
						.slice(0, 25),
				)
			}
		},
	}
}

type Autocompletion = {
	name: string
	getChoices: AutocompleteFunction
}

export type AutocompleteFunction = (
	value: string,
	interaction: Discord.AutocompleteInteraction<"cached">,
) => MaybePromise<Array<Discord.ApplicationCommandOptionChoiceData | string>>

class SetupState {
	private _options: SlashCommandOptionData[] = []
	private _autocompletions: Autocompletion[] = []
	private _currentInteraction?: Discord.ChatInputCommandInteraction<"cached">

	get options(): readonly SlashCommandOptionData[] {
		return this._options
	}

	get autocompletions(): readonly Autocompletion[] {
		return this._autocompletions
	}

	set currentInteraction(value: Discord.ChatInputCommandInteraction<"cached">) {
		this._currentInteraction = value
	}

	getCurrentInteraction(caller: (...args: unknown[]) => unknown) {
		if (!this._currentInteraction) {
			const error = new Error(
				"Option getters can only be called inside the run() function",
			)
			Error.captureStackTrace(error, caller)
			throw error
		}
		return this._currentInteraction
	}

	addOptionData(option: SlashCommandOptionData) {
		this._options.push(option)
	}

	addAutocompletion(autocompletion: Autocompletion) {
		this._autocompletions.push(autocompletion)
	}
}

type WithAutocompleteFunction<Args> = Omit<Args, "autocomplete"> & {
	autocomplete?: AutocompleteFunction
}

type StringOptionArgs = WithAutocompleteFunction<
	| Discord.ApplicationCommandStringOptionData
	| Discord.ApplicationCommandAutocompleteStringOptionData
>

type NumberOptionArgs = WithAutocompleteFunction<
	| Discord.ApplicationCommandNumericOptionData
	| Discord.ApplicationCommandAutocompleteNumericOptionData
>

export type SlashCommandSetupContext = ReturnType<typeof createSetupContext>

function createSetupContext(state: SetupState) {
	return {
		stringOption: createOptionFactory<StringOptionArgs>(state)(
			({ autocomplete, ...args }) => {
				if (autocomplete) {
					state.addAutocompletion({ name: args.name, getChoices: autocomplete })
				}
				return {
					data: {
						...args,
						type: Discord.ApplicationCommandOptionType.String,
						autocomplete: !!autocomplete,
					},
					getValue: (interaction) => interaction.options.getString(args.name),
				}
			},
		),

		numberOption: createOptionFactory<NumberOptionArgs>(state)(
			({ autocomplete, ...args }) => {
				if (autocomplete) {
					state.addAutocompletion({ name: args.name, getChoices: autocomplete })
				}
				return {
					data: {
						...args,
						type: Discord.ApplicationCommandOptionType.Number,
						autocomplete: !!autocomplete,
					},
					getValue: (interaction) => interaction.options.getNumber(args.name),
				}
			},
		),

		booleanOption:
			createOptionFactory<Discord.ApplicationCommandBooleanOptionData>(state)(
				(args) => ({
					data: {
						...args,
						type: Discord.ApplicationCommandOptionType.Boolean,
					},
					getValue: (interaction) => interaction.options.getBoolean(args.name),
				}),
			),

		channelOption:
			createOptionFactory<Discord.ApplicationCommandChannelOptionData>(state)(
				(args) => ({
					data: {
						...args,
						type: Discord.ApplicationCommandOptionType.Channel,
					},
					getValue: (interaction) => interaction.options.getChannel(args.name),
				}),
			),

		roleOption: createOptionFactory<Discord.ApplicationCommandRoleOptionData>(
			state,
		)((args) => ({
			data: {
				...args,
				type: Discord.ApplicationCommandOptionType.Role,
			},
			getValue: (interaction) => interaction.options.getRole(args.name),
		})),

		userOption: createOptionFactory<Discord.ApplicationCommandUserOptionData>(
			state,
		)((args) => ({
			data: {
				...args,
				type: Discord.ApplicationCommandOptionType.User,
			},
			getValue: (interaction) => interaction.options.getUser(args.name),
		})),

		mentionableOption:
			createOptionFactory<Discord.ApplicationCommandMentionableOptionData>(
				state,
			)((args) => ({
				data: {
					...args,
					type: Discord.ApplicationCommandOptionType.Mentionable,
				},
				getValue: (interaction) =>
					interaction.options.getMentionable(args.name),
			})),
	}
}

function createOptionFactory<Args extends { name: string }>(state: SetupState) {
	// the user shouldn't have to define type or required,
	// since that's implied based on the factory they use
	type UserArgs = Simplify<Omit<Args, "type" | "required">>

	return function withConfig<Value>(
		configure: (args: UserArgs) => {
			data: Extract<
				Discord.ApplicationCommandOptionData,
				{ required?: boolean }
			>
			getValue: (
				interaction: Discord.ChatInputCommandInteraction<"cached">,
			) => Value | null | undefined
		},
	) {
		function defineOption(args: UserArgs) {
			const config = configure(args)
			state.addOptionData(config.data)
			return function getValue() {
				return config.getValue(state.getCurrentInteraction(getValue))
			}
		}

		defineOption.required = function defineRequiredOption(args: UserArgs) {
			const config = configure(args)

			state.addOptionData({
				...config.data,
				required: true,
			})

			return function getRequiredValue() {
				const value = config.getValue(
					state.getCurrentInteraction(getRequiredValue),
				)
				if (value == null) {
					throw new Error(`Missing required option: ${args.name}`)
				}
				return value
			}
		}

		return defineOption
	}
}
