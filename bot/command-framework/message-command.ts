import {
	ApplicationCommandType,
	type MessageApplicationCommandData,
	type MessageContextMenuCommandInteraction,
} from "discord.js"
import type { StrictOmit } from "../helpers/types"
import type { Command } from "./command"

export type DefineMessageCommandArgs = StrictOmit<
	MessageApplicationCommandData,
	"type"
> & {
	run: (
		iunteraction: MessageContextMenuCommandInteraction<"cached">,
	) => Promise<void>
}

export function defineMessageCommand({
	run,
	...data
}: DefineMessageCommandArgs): Command {
	return {
		data: {
			...data,
			type: ApplicationCommandType.Message,
		},
		getAction: (interaction) => {
			if (interaction.commandName !== data.name) return
			if (!interaction.isMessageContextMenuCommand()) return
			return () => run(interaction)
		},
	}
}
