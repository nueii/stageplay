import {
	ApplicationCommandOptionType,
	type ApplicationCommandSubCommandData,
	ApplicationCommandType,
} from "discord.js"
import type { Command } from "./command"
import type { SlashCommand } from "./slash-command"

export function defineGroupCommand(
	name: string,
	description: string,
	commands: SlashCommand[],
): Command {
	return {
		data: {
			type: ApplicationCommandType.ChatInput,
			name,
			description,
			options: commands.map<ApplicationCommandSubCommandData>((command) => ({
				...command.data,
				type: ApplicationCommandOptionType.Subcommand,
			})),
		},
		getAction(interaction) {
			if (interaction.commandName !== name) {
				return
			}

			if (!interaction.isChatInputCommand() && !interaction.isAutocomplete()) {
				return
			}

			const subcommandName = interaction.options.getSubcommand(false)
			if (!subcommandName) return

			for (const command of commands) {
				const run = command.getAction(interaction, subcommandName)
				if (run) return run
			}
		},
	}
}
