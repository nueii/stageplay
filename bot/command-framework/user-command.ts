import {
	ApplicationCommandType,
	type UserApplicationCommandData,
	type UserContextMenuCommandInteraction,
} from "discord.js"
import type { StrictOmit } from "../helpers/types"
import type { Command } from "./command"

export type DefineUserCommandArgs = StrictOmit<
	UserApplicationCommandData,
	"type"
> & {
	run: (
		interaction: UserContextMenuCommandInteraction<"cached">,
	) => Promise<void>
}

export function defineUserCommand({
	run,
	...data
}: DefineUserCommandArgs): Command {
	return {
		data: {
			...data,
			type: ApplicationCommandType.User,
		},
		getAction: (interaction) => {
			if (interaction.commandName !== data.name) return
			if (!interaction.isUserContextMenuCommand()) return
			return () => run(interaction)
		},
	}
}
