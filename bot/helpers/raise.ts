export function raise(error: unknown): never {
	if (error instanceof Error) {
		Error.captureStackTrace(error)
	}
	throw error
}
