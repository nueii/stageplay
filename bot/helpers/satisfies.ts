export const satisfies =
	<T>() =>
	<V extends T>(value: V) =>
		value
