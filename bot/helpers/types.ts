export type StrictOmit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

export type ValueOf<T> = T[keyof T]

export type Simplify<T> = { [K in keyof T]: T[K] } & NonNullable<unknown>

export type MaybePromise<T> = T | PromiseLike<T>
