export async function resultify<T>(promise: PromiseLike<T> | T) {
	try {
		return [await promise, undefined] as const
	} catch (error) {
		return [undefined, error] as const
	}
}
