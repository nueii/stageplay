import { env } from "./core/env"

export const getFrontendUrl = (pathname: string) =>
	new URL(pathname, env.FRONTEND_URL || "http://localhost:3000").href
