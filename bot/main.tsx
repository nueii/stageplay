import "dotenv/config"

import chalk from "chalk"
import {
	ApplicationCommandOptionType,
	ApplicationCommandType,
	Client,
	GatewayIntentBits,
} from "discord.js"
import prettyMilliseconds from "pretty-ms"
import { ReacordDiscordJs } from "reacord"
import {
	setCommand,
	unsetCommand,
	whoamiCommand,
} from "./characters/channel-character-commands"
import { characterSuperCommand } from "./characters/character-commands"
import { characterInfoCommand } from "./characters/character-info-command"
import {
	getCharacterAndMessageFromPrefix,
	getCurrentCharacterFromMessage,
} from "./characters/common"
import { deleteMessageCommand } from "./characters/delete-message-command"
import { editMessageCommand } from "./characters/edit-message-command"
import { playCommand } from "./characters/play-command"
import { sendCharacterMessage } from "./characters/send-character-message"
import { type Command, runMatchingCommand } from "./command-framework/command"
import { defineSlashCommand } from "./command-framework/slash-command"
import { env } from "./core/env"
import { logger } from "./core/logger"
import { addEphemeralInteractionReply } from "./discord/add-interaction-response"
import { sendDirectMessageError } from "./discord/common"
import { InteractionError } from "./discord/interaction-error"
import { resultify } from "./helpers/resultify"

const client = new Client({
	intents: [
		GatewayIntentBits.Guilds,
		GatewayIntentBits.GuildMessages,
		GatewayIntentBits.MessageContent,
	],
})

const reacord = new ReacordDiscordJs(client)

const commands: Command[] = [
	defineSlashCommand(() => ({
		name: "ping",
		description: "pong",
		run: (interaction) => {
			const responseTime = prettyMilliseconds(
				Date.now() - interaction.createdTimestamp,
			)
			reacord.ephemeralReply(interaction, <>pong! ({responseTime})</>)
		},
	})),

	characterSuperCommand,
	playCommand,
	characterInfoCommand,
	editMessageCommand,
	deleteMessageCommand,

	setCommand,
	unsetCommand,
	whoamiCommand,
]

client.on("ready", async (client) => {
	try {
		const commandNameList = commands
			.flatMap((command) => {
				if (command.data.type !== ApplicationCommandType.ChatInput) {
					return command.data.name
				}

				const options = command.data.options ?? []

				const subcommandNames = options.flatMap((opt) =>
					opt.type === ApplicationCommandOptionType.Subcommand ? opt.name : [],
				)

				if (subcommandNames.length === 0) {
					return command.data.name
				}

				return subcommandNames.map(
					(name) => `${chalk.dim(command.data.name)} ${name}`,
				)
			})
			.map((command) => chalk.dim("- /") + command)

		await logger.promise(
			`Registering ${commandNameList.length} commands`,
			client.application.commands.set(commands.map((c) => c.data)),
		)

		logger.info(`Commands:\n${commandNameList.join("\n")}`)

		logger.info("Logged in as", client.user.username)
	} catch (error) {
		logger.error(error)
	}
})

client.on("interactionCreate", async (interaction) => {
	if (!interaction.inCachedGuild()) {
		if (interaction.isRepliable()) {
			await addEphemeralInteractionReply(
				interaction,
				"Sorry, this bot only works in servers!",
			)
		}
		return
	}

	if (!interaction.isCommand() && !interaction.isAutocomplete()) {
		return
	}

	try {
		await runMatchingCommand(interaction, commands)
	} catch (error) {
		if (!(error instanceof InteractionError)) {
			logger.error(error)
		}

		if (!interaction.isRepliable()) return

		await addEphemeralInteractionReply(
			interaction,
			error instanceof InteractionError
				? error.message
				: "Oops! Something went wrong. Try again?",
		)
	}
})

client.on("messageCreate", async (message) => {
	if (!message.inGuild()) return

	const [prefixResult, currentCharacterResult] = await Promise.allSettled([
		getCharacterAndMessageFromPrefix(message),
		getCurrentCharacterFromMessage(message),
	])

	if (prefixResult.status === "rejected") {
		logger.error("Get prefix failed:", prefixResult.reason)
	}
	if (currentCharacterResult.status === "rejected") {
		logger.error("Get current character failed:", currentCharacterResult.reason)
	}

	let character
	let characterMessage

	if (prefixResult.status === "fulfilled" && prefixResult.value) {
		character = prefixResult.value.character
		characterMessage = prefixResult.value.message
	}

	if (
		currentCharacterResult.status === "fulfilled" &&
		currentCharacterResult.value
	) {
		character = currentCharacterResult.value
		characterMessage = message.content.trim()
	}

	if (!character || !characterMessage) return

	const [, messageError] = await resultify(
		sendCharacterMessage(
			character,
			characterMessage,
			message.guild,
			message.channel,
			message.author,
		),
	)
	if (messageError) {
		sendDirectMessageError(message, messageError)
		return
	}

	const [, deleteError] = await resultify(message.delete())
	if (deleteError) {
		logger.error("Failed to delete message", deleteError)
		return
	}
})

export async function runBot() {
	await logger.promise("Logging in", client.login(env.DISCORD_BOT_TOKEN))
}
