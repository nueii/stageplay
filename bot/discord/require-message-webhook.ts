import type { Message } from "discord.js"
import { raise } from "../helpers/raise"
import { InteractionError } from "./interaction-error"

export function requireMessageWebhook(
	message: Message,
	errorMessage = "Message wasn't sent by a webhook",
): string {
	return message.webhookId ?? raise(new InteractionError(errorMessage))
}
