import type {
	BaseMessageOptions,
	InteractionReplyOptions,
	RepliableInteraction,
} from "discord.js"

export async function addInteractionResponse(
	interaction: RepliableInteraction,
	options: InteractionReplyOptions,
) {
	// if we add an ephemeral followup to a non-ephemeral defer, it edits the message (lol)
	if (interaction.deferred && !interaction.ephemeral && options.ephemeral) {
		await interaction.deleteReply()
	}
	if (interaction.deferred && interaction.ephemeral && options.ephemeral) {
		return interaction.editReply(options)
	}
	if (interaction.replied || interaction.deferred) {
		return interaction.followUp(options)
	}
	return interaction.reply(options)
}

export async function addEphemeralInteractionReply(
	interaction: RepliableInteraction,
	optionsOrContent: string | InteractionReplyOptions,
) {
	let options: InteractionReplyOptions
	if (typeof optionsOrContent === "string") {
		options = { content: optionsOrContent }
	} else {
		options = optionsOrContent
	}

	// if we add an ephemeral followup to a non-ephemeral defer, it edits the message (lol)
	if (interaction.deferred && !interaction.ephemeral) {
		await interaction.deleteReply()
	}
	if (interaction.replied || interaction.deferred) {
		return interaction.followUp({ ...options, ephemeral: true })
	}
	return interaction.reply({ ...options, ephemeral: true, fetchReply: true })
}

export async function updateEphemeralInteractionReply(
	interaction: RepliableInteraction,
	options: BaseMessageOptions,
) {
	// if we add an ephemeral followup to a non-ephemeral defer, it edits the message (lol)
	if (interaction.deferred && !interaction.ephemeral) {
		await interaction.deleteReply()
	}
	if (interaction.isMessageComponent()) {
		return interaction.update(options)
	}
	if (interaction.replied || interaction.deferred) {
		return interaction.editReply(options)
	}
	return interaction.reply({ ...options, ephemeral: true, fetchReply: true })
}
