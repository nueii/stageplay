import { randomUUID } from "node:crypto"
import type { RepliableInteraction } from "discord.js"
import { ButtonStyle, ComponentType } from "discord.js"
import { updateEphemeralInteractionReply } from "./add-interaction-response"

export async function requestConfirmation({
	interaction,
	message,
	confirmLabel,
	confirmStyle,
}: {
	interaction: RepliableInteraction
	message: string
	confirmLabel: string
	confirmStyle:
		| ButtonStyle.Primary
		| ButtonStyle.Secondary
		| ButtonStyle.Success
		| ButtonStyle.Danger
}) {
	const cancelId = `cancel-${randomUUID()}`
	const confirmId = `confirm-${randomUUID()}`

	const response = await updateEphemeralInteractionReply(interaction, {
		content: message,
		embeds: [],
		components: [
			{
				type: ComponentType.ActionRow,
				components: [
					{
						type: ComponentType.Button,
						label: "Cancel",
						style: ButtonStyle.Secondary,
						customId: cancelId,
					},
					{
						type: ComponentType.Button,
						label: confirmLabel,
						style: confirmStyle,
						customId: confirmId,
					},
				],
			},
		],
	})

	const componentInteraction = await response.awaitMessageComponent({
		componentType: ComponentType.Button,
		filter: (i) => {
			return i.customId === cancelId || i.customId === confirmId
		},
	})

	return {
		answer: componentInteraction.customId === confirmId,
		interaction: componentInteraction,
	}
}
