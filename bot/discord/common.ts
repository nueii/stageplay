import type { Message } from "discord.js"
import { logger } from "../core/logger"
import { toError } from "../helpers/to-error"

export function sendDirectMessageError(message: Message<true>, error: unknown) {
	logger.error(message.id, error)

	const { message: errorMessage, stack } = toError(error)

	message.author
		.send({
			content: [
				"Oops! Something went wrong. Please contact the bot owner.",
				codeBlock(stack || errorMessage),
				codeBlock(`Message ID: ${message.id}`),
			].join("\n"),
		})
		.catch((error) => {
			logger.error(message.id, "Failed to send error message to user", error)
		})
}

function codeBlock(text: string) {
	return `\`\`\`\n${text}\n\`\`\``
}
