import type {
	AwaitModalSubmitOptions,
	CommandInteraction,
	MessageComponentInteraction,
	ModalComponentData,
	ModalSubmitInteraction,
	TextInputComponentData,
} from "discord.js"
import { ComponentType } from "discord.js"

export type FieldConfig = Omit<TextInputComponentData, "type" | "customId">

export type FieldConfigMap = Record<string, FieldConfig>

export type ModalInteraction = MessageComponentInteraction | CommandInteraction

export class Modal<Fields extends FieldConfigMap> {
	constructor(
		readonly customId: string,
		readonly fields: Fields,
	) {}

	data(
		title: string,
		initialValues: Partial<Record<keyof Fields, string>> = {},
	): ModalComponentData {
		return {
			title,
			customId: this.customId,
			components: Object.entries(this.fields).map(([key, field]) => ({
				type: ComponentType.ActionRow,
				components: [
					{
						...field,
						type: ComponentType.TextInput,
						customId: key,
						value: initialValues[key],
						required: field.required ?? false,
					},
				],
			})),
		}
	}

	async submission(
		interaction: ModalInteraction,
		options: Partial<AwaitModalSubmitOptions<ModalSubmitInteraction>> = {},
	) {
		try {
			return await interaction.awaitModalSubmit({
				time: 1000 * 60 * 30,
				filter: (interaction_2) => interaction_2.customId === this.customId,
				...options,
			})
			// eslint-disable-next-line no-empty
		} catch {}
	}

	getResponseData(interaction: ModalSubmitInteraction) {
		const data = {} as Record<keyof Fields, string>
		for (const key in this.fields) {
			data[key] = interaction.fields.getTextInputValue(key)
		}
		return data
	}
}
