import type { Interaction } from "discord.js"
import { raise } from "../helpers/raise"
import { InteractionError } from "./interaction-error"

export function requireInteractionGuild(interaction: Interaction) {
	return (
		interaction.guild ??
		raise(new InteractionError("This command can only be used in a server"))
	)
}
