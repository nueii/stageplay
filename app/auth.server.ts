import { createClerkClient } from "@clerk/remix/api.server"
import { getAuth } from "@clerk/remix/ssr.server"
import type { DataFunctionArgs } from "@remix-run/node"
import { env } from "./env.server.ts"
import { raise } from "./helpers/errors"
import { unauthorized } from "./helpers/responses.ts"

export async function getUser(args: DataFunctionArgs) {
	const auth = await getAuth(args)
	if (!auth.userId) return

	const clerk = createClerkClient({ secretKey: env.CLERK_SECRET_KEY })
	const user = await clerk.users.getUser(auth.userId)

	const discordAccount =
		user.externalAccounts.find(
			(account) => account.provider === "oauth_discord",
		) ?? raise(unauthorized("No discord account linked"))

	return {
		clerkId: user.id,
		discordId: discordAccount.externalId,
		name: user.username ?? "unnamed",
		auth,
	}
}

export async function requireUser(args: DataFunctionArgs) {
	const user = await getUser(args)
	return user ?? raise(new Response(undefined, { status: 401 }))
}
