import { PrismaClient } from "@prisma/client"

declare global {
	// biome-ignore lint/style/noVar: can't use globalThis with let
	var __prismaClient: PrismaClient | undefined
}

globalThis.__prismaClient ??= new PrismaClient()
export const db = globalThis.__prismaClient
