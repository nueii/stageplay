import type { MetaDescriptor } from "@remix-run/node"

const appTitle = "Stageplay"

const appDescription =
	"Stageplay is a Discord app that makes it easier to put on the show of your dreams. Create characters and send messages as if you were really there!"

const appUrl = "https://stageplay.nuei.dev"

export function getMeta(
	options: {
		title?: string
		description?: string
		image?: string
	} = {},
): MetaDescriptor[] {
	const title = options.title ? `${options.title} | ${appTitle}` : appTitle
	const description = options.description ?? appDescription

	return [
		{ title },
		{ name: "description", content: description },

		{ property: "og:title", content: title },
		{ property: "og:description", content: description },
		{ property: "og:type", content: "website" },
		{ property: "og:url", content: appUrl },
		{ property: "og:site_name", content: appTitle },

		{ name: "twitter:title", content: title },
		{ name: "twitter:description", content: description },
		{
			name: "twitter:card",
			content: options.image ? "summary_large_image" : "summary",
		},
		options.image && { name: "twitter:image", content: options.image },
	].filter(Boolean)
}
