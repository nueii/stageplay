import { type ClassNameValue, twMerge } from "tailwind-merge"

export const defineStyle =
	(className: string) =>
	(...classes: ClassNameValue[]) =>
		twMerge(className, ...classes)
