import { defineStyle } from "./defineStyle.ts"

export const container = defineStyle(
	"mx-auto w-full max-w-screen-md p-2 sm:p-4",
)

export const pageHeading = defineStyle("text-3xl font-light")

export const skeletonBlock = defineStyle("bg-black/50 animate-pulse")

export const errorText = defineStyle("text-red-400")
