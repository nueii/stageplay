export function getCharacterProfileUrl(name: string) {
	return `https://www.f-list.net/c/${encodeURIComponent(name.toLowerCase())}/`
}

export function getCharacterAvatarUrl(name: string) {
	return `https://static.f-list.net/images/avatar/${encodeURIComponent(
		name.toLowerCase(),
	)}.png`
}

export function getIconUrl(name: string) {
	return `https://static.f-list.net/images/eicon/${encodeURIComponent(
		name.toLowerCase(),
	)}.gif`
}
