/**
 * Throws the given value. If it's a string, wraps it in an error instance.
 * Useful for raising errors in expression positions
 *
 * @param errorInput The value to throw. If this is a string, it will be wrapped
 *   in an error instance
 * @param caller The function that is calling this function. This is used to
 *   capture a stack trace
 */
export function raise(
	errorInput: unknown,
	caller?: (...args: never[]) => unknown,
): never {
	let error = errorInput
	if (typeof error === "string") {
		error = new Error(error)
	}
	if (error instanceof Error && caller) {
		Error.captureStackTrace(error, caller)
	}
	throw error
}
