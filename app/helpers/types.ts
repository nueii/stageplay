export type Nullish<T> = T | null | undefined

export type MaybePromise<T> = T | PromiseLike<T>

export type Overwrite<A extends object, B extends object> = Simplify<
	Omit<A, keyof B> & B
>

// eslint-disable-next-line @typescript-eslint/ban-types
export type Simplify<T> = { [K in keyof T]: T[K] } & {}

export type EntryOf<T> = {
	[K in keyof T]: readonly [K, T[K]]
}[keyof T]

export type Falsy = false | 0 | "" | null | undefined
