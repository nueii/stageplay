import DOMPurify from "isomorphic-dompurify"
import { Renderer, marked } from "marked"

const allowedTags = [
	"a",
	"big",
	"blockquote",
	"center",
	"code",
	"em",
	"h1",
	"h2",
	"h3",
	"h4",
	"h5",
	"h6",
	"hr",
	"img",
	"left",
	"li",
	"ol",
	"p",
	"pre",
	"right",
	"s",
	"small",
	"strong",
	"sub",
	"sup",
	"table",
	"tbody",
	"td",
	"th",
	"thead",
	"tr",
	"ul",
	"del",
	"br",
	"dl",
	"dt",
	"dd",
	"details",
	"summary",
]

const allowedAttributeNames = [
	"href",
	"target",
	"rel",
	"src",
	"alt",
	"width",
	"height",
	"title",
]

class CustomRenderer extends Renderer {
	link(href: string, title: string | null | undefined, text: string): string {
		const attributes: Record<string, string> = {
			href,
			target: "_blank",
			rel: "noopener noreferrer",
		}
		if (title) {
			attributes.title = title
		}

		const attributeString = Object.entries(attributes)
			.map(([key, value]) => `${key}="${value}"`)
			.join(" ")

		return `<a ${attributeString}>${text}</a>`
	}
}

const renderer = new CustomRenderer()

export function renderMarkdown(markdown: string): { __html: string } {
	let html

	html = marked.parse(markdown, {
		gfm: true,
		breaks: false,
		renderer,
	})

	if (typeof html !== "string") {
		// we can't handle promises yet
		return { __html: markdown }
	}

	html = DOMPurify.sanitize(html, {
		ALLOWED_TAGS: allowedTags,
		ALLOWED_ATTR: allowedAttributeNames,
		RETURN_DOM_FRAGMENT: false,
		RETURN_DOM: false,
	})

	return { __html: html.trim() }
}
