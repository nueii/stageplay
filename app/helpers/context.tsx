import { createContext, useContext } from "react"

const empty = Symbol()

export function createContextWrapper<Props extends object, Value>(
	name: string,
	useProvider: (props: Props) => Value,
) {
	const Context = createContext<Value | typeof empty>(empty)

	function Provider(props: Props & { children: React.ReactNode }) {
		const value = useProvider(props)
		return <Context.Provider value={value}>{props.children}</Context.Provider>
	}
	Provider.displayName = `Provider(${name})`

	function useValue() {
		const value = useContext(Context)
		if (value === empty) {
			throw new Error(`Provider not found for ${name}`)
		}
		return value
	}

	return [Provider, useValue] as const
}
