import * as React from "react"

export type Slot<Props> =
	| React.ReactElement<Props>
	| ((props: Props) => React.ReactNode)

export function renderSlot<Props>(
	slot: Slot<Props>,
	props: Props & React.Attributes,
) {
	return React.isValidElement(slot)
		? React.cloneElement(slot, props)
		: slot(props)
}
