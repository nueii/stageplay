import { expect, test } from "vitest"
import { parseBBCode } from "./parse-bbcode"

test("plain text", () => {
	expect(parseBBCode("plain text")).toMatchInlineSnapshot(`
    [
      {
        "text": "plain text",
        "type": "text",
      },
    ]
  `)
})

test("missing end tags", () => {
	expect(parseBBCode("[b][i]text")).toMatchInlineSnapshot(`
    [
      {
        "children": [
          {
            "children": [
              {
                "text": "text",
                "type": "text",
              },
            ],
            "innerText": "text",
            "outerText": "[i]text",
            "tag": "i",
            "type": "tag",
            "value": undefined,
          },
        ],
        "innerText": "[i]text",
        "outerText": "[b][i]text",
        "tag": "b",
        "type": "tag",
        "value": undefined,
      },
    ]
  `)
})

test("mismatched end tag", () => {
	expect(parseBBCode("[b]bold[/i]")).toMatchInlineSnapshot(`
    [
      {
        "children": [
          {
            "text": "bold[/i]",
            "type": "text",
          },
        ],
        "innerText": "bold[/i]",
        "outerText": "[b]bold[/i]",
        "tag": "b",
        "type": "tag",
        "value": undefined,
      },
    ]
  `)
})

test("swapped end tags", () => {
	expect(parseBBCode("[b]bold[/i][/b]")).toMatchInlineSnapshot(`
    [
      {
        "children": [
          {
            "text": "bold[/i]",
            "type": "text",
          },
        ],
        "innerText": "bold[/i]",
        "outerText": "[b]bold[/i][/b]",
        "tag": "b",
        "type": "tag",
        "value": undefined,
      },
    ]
  `)
})

test("kitchen sink", () => {
	const input = "[b]bold [i]italic[/i] [url=https://example.com]link[/url]"

	expect(parseBBCode(input)).toMatchInlineSnapshot(`
    [
      {
        "children": [
          {
            "text": "bold ",
            "type": "text",
          },
          {
            "children": [
              {
                "text": "italic",
                "type": "text",
              },
            ],
            "innerText": "italic",
            "outerText": "[i]italic[/i]",
            "tag": "i",
            "type": "tag",
            "value": undefined,
          },
          {
            "text": " ",
            "type": "text",
          },
          {
            "children": [
              {
                "text": "link",
                "type": "text",
              },
            ],
            "innerText": "link",
            "outerText": "[url=https://example.com]link[/url]",
            "tag": "url",
            "type": "tag",
            "value": "https://example.com",
          },
        ],
        "innerText": "bold [i]italic[/i] [url=https://example.com]link[/url]",
        "outerText": "[b]bold [i]italic[/i] [url=https://example.com]link[/url]",
        "tag": "b",
        "type": "tag",
        "value": undefined,
      },
    ]
  `)
})
