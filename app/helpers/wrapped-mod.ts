/** Modulo that wraps with negative numbers */
export function wrappedMod(num: number, radix: number): number {
	return ((num % radix) + radix) % radix
}
