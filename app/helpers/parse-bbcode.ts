export type BBCodeNode =
	| { type: "text"; text: string }
	| {
			type: "tag"
			tag: string
			value: string | undefined
			children: BBCodeNode[]
			outerText: string
			innerText: string
	  }

const tagRegex = /\[([a-z]+)(?:=([^\]]+))?]/i

export function parseBBCode(text: string) {
	const nodes: BBCodeNode[] = []
	let position = 0

	while (position < text.length) {
		const currentText = text.slice(position)
		const tagMatch = currentText.match(tagRegex)
		if (!tagMatch) {
			nodes.push({ type: "text", text: currentText })
			break
		}

		const [tag, tagName, tagValue] = tagMatch
		if (!tagName) {
			throw new Error("Unexpected: tagName is undefined")
		}
		if (tagMatch.index === undefined) {
			throw new Error("Unexpected: tagMatch.index is undefined")
		}

		const textBeforeTag = currentText.slice(0, tagMatch.index)
		if (textBeforeTag.length > 0) {
			nodes.push({ type: "text", text: textBeforeTag })
		}

		let endTag = `[/${tagName}]`
		let endTagPosition = currentText.indexOf(endTag)
		if (endTagPosition === -1) {
			endTag = ""
			endTagPosition = currentText.length
		}

		const tagContent = currentText.slice(
			tagMatch.index + tag.length,
			endTagPosition,
		)
		const children = parseBBCode(tagContent)

		nodes.push({
			type: "tag",
			tag: tagName,
			value: tagValue,
			children,
			outerText: tag + tagContent + endTag,
			innerText: tagContent,
		})

		position +=
			textBeforeTag.length + tag.length + tagContent.length + endTag.length
	}

	return nodes
}
