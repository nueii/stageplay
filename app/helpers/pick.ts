export function pick<T, K extends keyof T>(obj: T, keys: K[]) {
	const result = {} as Pick<T, K>
	for (const key of keys) {
		result[key] = obj[key]
	}
	return result
}
