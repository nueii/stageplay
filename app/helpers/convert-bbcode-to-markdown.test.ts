import { expect, test } from "vitest"
import { bbcodeToMarkdown } from "./convert-bbcode-to-markdown"
import { parseBBCode } from "./parse-bbcode"

test("kitchen sink", () => {
	// test al tags from https://wiki.f-list.net/List_of_BBCode_tags
	const input = `
[b]bold[/b]
[i]italic[/i]
[u]underline[/u]
[s]strikethrough[/s]
[big]big text[/big]
[small]small text[/small]
[sub]subscript[/sub]
[sup]superscript[/sup]
[color=red]red text[/color]
[url]https://www.google.com/[/url]
[url=https://www.google.com/]google[/url]
[heading]heading[/heading]
[indent]indented text[/indent]
[justify]justified text[/justify]
[collapse=summary]collapsed text[/collapse]
[quote]quote[/quote]
[hr]
[left]left-aligned text[/left]
[center]centered text[/center]
[right]right-aligned text[/right]
[icon]name[/icon]
[eicon]name[/eicon]
[user]name[/user]
[noparse][b]bold[/b][/noparse]
[img=123456]unsupported[/img]
`

	expect(bbcodeToMarkdown(parseBBCode(input))).toMatchInlineSnapshot(`
    "**bold**
    *italic*
    __underline__
    ~~strikethrough~~
    <big>big text</big>
    <small>small text</small>
    <sub>subscript</sub>
    <sup>superscript</sup>
    red text
    [https://www.google.com/](https://www.google.com/)
    [google](https://www.google.com/)

    # heading

    indented text
    justified text

    ## summary

    collapsed text

    > quote

    ---

    left-aligned text
    <center>centered text</center>
    right-aligned text
    [![name](https://static.f-list.net/images/avatar/name.png)](https://www.f-list.net/c/name/)
    ![name](https://static.f-list.net/images/eicon/name.gif)
    [name](https://www.f-list.net/c/name/)
    [b]bold[/b]"
  `)
})

test("multiple lines", () => {
	const input = `
[b]bold
bold[/b]

[i]italic
[b]bold[/b]
[u]underline[/u]
[/i]`

	expect(bbcodeToMarkdown(parseBBCode(input))).toMatchInlineSnapshot(`
    "**bold
    bold**

    *italic
    **bold**
    __underline__*"
  `)
})

test("correctly formatting headers", () => {
	const input = `
[b][heading]heading[/heading][/b]
[i][heading]heading[/heading][/i]
[u][heading]heading[/heading][/u]
[s][heading]heading[/heading][/s]
[quote][heading]heading[/heading][/quote]`

	expect(bbcodeToMarkdown(parseBBCode(input))).toMatchInlineSnapshot(`
    "# **heading**
    # *heading*
    # __heading__
    # ~~heading~~
    > # heading"
  `)
})

test("subaru", () => {
	const input = `
[center][img=2513093]subaru[/img][/center][hr][i][color=white][collapse=My Job]Officially, I'm an adventurer... in other words, a glorified errand runner.
I kill things, fetch things, deliver things, and escort things, usually for absurd sums of money.

It makes good money, at least, and it makes me feel useful.
Good for taking my mind off of things too, whenever I need it.

[small]...I guess it's kind of fun, too.[/small]
[/collapse]
[collapse=My Outfit]I was kinda picky with gear when I first started out.
It took me a while to find an outfit, weapon and fighting style that felt just right, but I guess it's the same way for others.
Eventually, I stumbled on this cool-looking cloak that felt really nice to move around in, compared to the clunky armor I tried before.
And... well. [small]I guess it made me feel cool too.[/small]

Since it was too big [small]like most things I tried[/small], I had an outfit custom-made, basically what you're seeing now.
It's even enchanted. It lets me turn invisible for a short amount of time, and passively helps me feel even lighter and swifter.
It wasn't cheap, but still worth.
[/collapse]
[collapse=My Dad]I lived in a normal, peaceful neighborhood, with my dad.
He was a good dad. He acted gentle with me whenever I did anything wrong, and didn't yell.
He always offered a shoulder and opened himself to talk whenever I needed it.
I made sure to give him a hug every time he came home.

Growing up, I studied every day, did my homework, spent my spare time reading and not much else.
I was just a good kid who always did as she was told. Teacher's pet and all that.

...One day, my dad got really sick. He tried to hide it, but it became harder to ignore as time went on.
I started looking for ways to earn money for a cure, and eventually, I settled on training to become an adventurer.
Others my age were doing the same thing, and I heard it paid well, so I followed along, jumped into it and hoped for the best.

Unfortunately, I wasn't particularly good at it, even now. But it was something.
I trained hard, and I trained every day.
It was... painful. And a little embarrassing, being made fun of because I wasn't as strong or agile as the others.
But I didn't care. As long as I could be good enough, I could help him, I thought.
I could probably make enough in time, I thought.

[small]...I thought.[/small]

Younger me was too naive. Maybe that was just life's way of beating reality into me.
It's been a year now, and... I think I've come to peace with it now.
I don't feel sad about him being gone. I'm happy, actually.
Happy to have had him as a dad.
[/collapse]
[collapse=My Gender...?]
Well, sometimes, people do think I'm a guy.
I don't really care that much. And... I guess it's kinda fun to act like it. [small]A little.[/small]

But don't call me cute. [b]I'm not cute.[/b] [small]I'll stab you.[/small]
[/collapse]
[collapse=M-my Relationship...]
I've never really thought about it a lot.
The thought of asking someone out makes me feel... nervous.
Same if they were asking me out, but... someone like me? [small]Why?[/small]

I don't really want to worry about any of that.
I just want to relax, and occasionally stab stuff.

[small]...I guess if the right guy came around, or... even a girl, maybe I'd consider. Maybe.[/small]
[/collapse][/color][/i][hr]
[color=white][i]Art belongs to [url=https://twitter.com/ookaka7]this amazing artist[/url], and is only used for reference.

Default appearances:
[url=https://static.f-list.net/images/charimage/25325364.jpg]Inside lounging outfit[/url]
[url=https://static.f-list.net/images/charimage/25325359.jpg]Outside outfit[/url]
[/i][/color]`

	expect(bbcodeToMarkdown(parseBBCode(input))).toMatchInlineSnapshot(`
    "<center></center>

    ---

    ## *My Job*

    *Officially, I'm an adventurer... in other words, a glorified errand runner.
    I kill things, fetch things, deliver things, and escort things, usually for absurd sums of money.*

    *It makes good money, at least, and it makes me feel useful.
    Good for taking my mind off of things too, whenever I need it.*

    *<small>...I guess it's kind of fun, too.</small>*

    ## *My Outfit*

    *I was kinda picky with gear when I first started out.
    It took me a while to find an outfit, weapon and fighting style that felt just right, but I guess it's the same way for others.
    Eventually, I stumbled on this cool-looking cloak that felt really nice to move around in, compared to the clunky armor I tried before.
    And... well. <small>I guess it made me feel cool too.</small>*

    *Since it was too big <small>like most things I tried</small>, I had an outfit custom-made, basically what you're seeing now.
    It's even enchanted. It lets me turn invisible for a short amount of time, and passively helps me feel even lighter and swifter.
    It wasn't cheap, but still worth.*

    ## *My Dad*

    *I lived in a normal, peaceful neighborhood, with my dad.
    He was a good dad. He acted gentle with me whenever I did anything wrong, and didn't yell.
    He always offered a shoulder and opened himself to talk whenever I needed it.
    I made sure to give him a hug every time he came home.*

    *Growing up, I studied every day, did my homework, spent my spare time reading and not much else.
    I was just a good kid who always did as she was told. Teacher's pet and all that.*

    *...One day, my dad got really sick. He tried to hide it, but it became harder to ignore as time went on.
    I started looking for ways to earn money for a cure, and eventually, I settled on training to become an adventurer.
    Others my age were doing the same thing, and I heard it paid well, so I followed along, jumped into it and hoped for the best.*

    *Unfortunately, I wasn't particularly good at it, even now. But it was something.
    I trained hard, and I trained every day.
    It was... painful. And a little embarrassing, being made fun of because I wasn't as strong or agile as the others.
    But I didn't care. As long as I could be good enough, I could help him, I thought.
    I could probably make enough in time, I thought.*

    *<small>...I thought.</small>*

    *Younger me was too naive. Maybe that was just life's way of beating reality into me.
    It's been a year now, and... I think I've come to peace with it now.
    I don't feel sad about him being gone. I'm happy, actually.
    Happy to have had him as a dad.*

    ## *My Gender...?*

    *Well, sometimes, people do think I'm a guy.
    I don't really care that much. And... I guess it's kinda fun to act like it. <small>A little.</small>*

    *But don't call me cute. **I'm not cute.** <small>I'll stab you.</small>*

    ## *M-my Relationship...*

    *I've never really thought about it a lot.
    The thought of asking someone out makes me feel... nervous.
    Same if they were asking me out, but... someone like me? <small>Why?</small>*

    *I don't really want to worry about any of that.
    I just want to relax, and occasionally stab stuff.*

    *<small>...I guess if the right guy came around, or... even a girl, maybe I'd consider. Maybe.</small>*

    ---

    *Art belongs to [this amazing artist](https://twitter.com/ookaka7), and is only used for reference.*

    *Default appearances:
    [Inside lounging outfit](https://static.f-list.net/images/charimage/25325364.jpg)
    [Outside outfit](https://static.f-list.net/images/charimage/25325359.jpg)*"
  `)
})
