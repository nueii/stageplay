import { sleep } from "./sleep"

export function delay(ms: number) {
	return async <T>(value: T) => {
		await sleep(ms)
		return value
	}
}
