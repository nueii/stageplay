type RangeArgs =
	| [len: number]
	| [start: number, end: number]
	| [start: number, end: number, step: number]

export function range(...args: RangeArgs) {
	let start = 0
	let end = 0
	let step = 1

	if (args.length === 1) {
		end = args[0]
	} else if (args.length === 2) {
		start = args[0]
		end = args[1]
	} else {
		start = args[0]
		end = args[1]
		step = args[2]
	}

	const result = []
	for (let i = start; i < end; i += step) {
		result.push(i)
	}
	return result
}
