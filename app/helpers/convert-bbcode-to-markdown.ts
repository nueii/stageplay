import type { BBCodeNode } from "~/helpers/parse-bbcode"
import {
	getCharacterAvatarUrl,
	getCharacterProfileUrl,
	getIconUrl,
} from "./f-list"

export function bbcodeToMarkdown(nodes: BBCodeNode[]): string {
	return nodes
		.map(getNodeText)
		.join("")
		.replaceAll(/\n{3,}/g, "\n\n")
		.trim()
}

function getNodeText(node: BBCodeNode): string {
	if (node.type === "text") {
		return node.text
	}

	switch (node.tag) {
		case "b": {
			return wrapChunks(bbcodeToMarkdown(node.children), "**", "**")
		}
		case "i": {
			return wrapChunks(bbcodeToMarkdown(node.children), "*", "*")
		}
		case "u": {
			return wrapChunks(bbcodeToMarkdown(node.children), "__", "__")
		}
		case "s": {
			return wrapChunks(bbcodeToMarkdown(node.children), "~~", "~~")
		}
		case "big": {
			return `<big>${bbcodeToMarkdown(node.children)}</big>`
		}
		case "small": {
			return `<small>${bbcodeToMarkdown(node.children)}</small>`
		}
		case "sub": {
			return `<sub>${bbcodeToMarkdown(node.children)}</sub>`
		}
		case "sup": {
			return `<sup>${bbcodeToMarkdown(node.children)}</sup>`
		}
		case "color": {
			return bbcodeToMarkdown(node.children)
		}
		case "url": {
			return `[${node.innerText}](${node.value || node.innerText})`
		}
		case "heading": {
			return `\n# ${bbcodeToMarkdown(node.children)}\n\n`
		}
		case "indent": {
			return bbcodeToMarkdown(node.children)
		}
		case "justify": {
			return bbcodeToMarkdown(node.children)
		}
		case "collapse": {
			return `\n## ${node.value || ""}\n\n${bbcodeToMarkdown(
				node.children,
			)}\n\n`
		}
		case "quote": {
			return bbcodeToMarkdown(node.children)
				.split(/\r?\n/)
				.map((line) => `> ${line}`)
				.join("\n")
		}
		case "hr": {
			return `\n\n---\n\n${bbcodeToMarkdown(node.children)}`
		}
		case "left": {
			return bbcodeToMarkdown(node.children)
		}
		case "center": {
			return `<center>${bbcodeToMarkdown(node.children)}</center>`
		}
		case "right": {
			return bbcodeToMarkdown(node.children)
		}
		case "icon": {
			const avatarUrl = getCharacterAvatarUrl(node.innerText)
			const avatar = `![${node.innerText}](${avatarUrl})`
			return `[${avatar}](${getCharacterProfileUrl(node.innerText)})`
		}
		case "eicon": {
			return `![${node.innerText}](${getIconUrl(node.innerText)})`
		}
		case "user": {
			return `[${node.innerText}](${getCharacterProfileUrl(node.innerText)})`
		}
		case "noparse": {
			return node.innerText
		}
		case "img": {
			return ""
		}
	}

	return node.outerText
}

function wrapChunks(text: string, open: string, close: string) {
	return text
		.split(/(?:\r?\n){2}/)
		.map((chunk) => chunk.trim())
		.map((chunk) => {
			if (!chunk) return ""
			if (chunk === "---") return chunk

			const [prefix, ...rest] = chunk.split(" ") as [string, ...string[]]
			const isHeader = /^#+$/.test(prefix)
			if (isHeader) {
				return `${prefix} ${open}${rest.join(" ")}${close}`
			}

			return `${open}${chunk}${close}`
		})
		.join("\n\n")
}
