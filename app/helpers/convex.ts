import { useQuery } from "convex/react"
import type { FunctionReference, OptionalRestArgs } from "convex/server"
import { useState } from "react"

export function useStableQuery<F extends FunctionReference<"query", "public">>(
	func: F,
	...args: OptionalRestArgs<F>
) {
	const data = useQuery(func, ...args)
	const [stableData, setStableData] = useState(data)
	if (data !== stableData && data !== undefined) {
		setStableData(data)
	}
	const pending = data !== stableData
	return [stableData, pending] as const
}
