export const notFound = (statusText?: string) =>
	new Response(undefined, { status: 404, statusText })

export const unauthorized = (statusText?: string) =>
	new Response(undefined, { status: 401, statusText })

export const forbidden = (statusText?: string) =>
	new Response(undefined, { status: 403, statusText })
