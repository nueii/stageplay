import { useState } from "react"

type AsyncState<Data> =
	| { status: "idle" }
	| { status: "loading" }
	| { status: "success"; data: Awaited<Data> }
	| { status: "error"; error: unknown }

export function useAsyncCallback<Args extends unknown[], Data>(
	callback: (...args: Args) => Data,
) {
	const [state, setState] = useState<AsyncState<Data>>({ status: "idle" })

	async function run(...args: Args) {
		if (state.status === "loading") return
		setState({ status: "loading" })
		try {
			setState({ status: "success", data: await callback(...args) })
		} catch (error) {
			setState({ status: "error", error })
		}
	}

	return [run, state] as const
}
