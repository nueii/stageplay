import { useCallback, useEffect, useRef } from "react"

export function useDebouncedCallback<Args extends unknown[]>(
	callback: (...args: Args) => void,
	period: number,
) {
	const callbackRef = useRef<typeof callback>(() => {
		throw new Error("Attempted to call debounced callback while rendering")
	})
	useEffect(() => {
		callbackRef.current = callback
	})

	const timeoutRef = useRef<ReturnType<typeof setTimeout>>()
	return useCallback(
		(...args: Args) => {
			clearTimeout(timeoutRef.current)
			timeoutRef.current = setTimeout(() => {
				callbackRef.current(...args)
			}, period)
		},
		[period],
	)
}
