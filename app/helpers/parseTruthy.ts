import type { Falsy } from "./types.ts"

export function parseTruthy<T>(value: T | Falsy): T {
	if (!value) {
		const error = new Error("Value is not truthy")
		Error.captureStackTrace(error, parseTruthy)
		throw error
	}
	return value
}
