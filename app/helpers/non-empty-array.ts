export type NonEmptyArray<T> = [T, ...T[]]

export function isNonEmptyArray<T>(value: T[]): value is NonEmptyArray<T> {
	return Array.isArray(value) && value.length > 0
}
