import "@fontsource-variable/sofia-sans/index.css"
import "./tailwind.css"

import { ClerkApp, ClerkErrorBoundary, useAuth } from "@clerk/remix"
import { rootAuthLoader } from "@clerk/remix/ssr.server"
import type { LoaderFunctionArgs, MetaFunction } from "@remix-run/node"
import {
	Links,
	LiveReload,
	Meta,
	Outlet,
	Scripts,
	ScrollRestoration,
	useLoaderData,
} from "@remix-run/react"
import { ConvexReactClient } from "convex/react"
import { ConvexProviderWithClerk } from "convex/react-clerk"
import { useState } from "react"
import { AppFooter } from "~/components/app-footer.tsx"
import background from "./assets/bocchi-bg.png"
import icon from "./assets/venetian-mask.svg?url"
import { TooltipProvider } from "./components/Tooltip.tsx"
import { AppHeader } from "./components/app-header.tsx"
import { env } from "./env.server.ts"
import { getMeta } from "./meta.ts"

export function loader(args: LoaderFunctionArgs) {
	return rootAuthLoader(args, () => ({
		convexUrl: env.CONVEX_URL,
		clerkAccountPortalUrl: env.CLERK_ACCOUNT_PORTAL_URL,
		pushPublicKey: env.PUSH_PUBLIC_KEY,
	}))
}

export const meta: MetaFunction = () => getMeta()

function Document({ children }: { children: React.ReactNode }) {
	return (
		<html
			lang="en"
			className="break-words bg-shade-900 text-accent-50 [word-break:break-word]"
		>
			<head>
				<meta charSet="utf-8" />
				<meta name="viewport" content="width=device-width,initial-scale=1" />
				<meta name="theme-color" content="#d8b4fe" />
				<link rel="icon" href={icon} />
				<Meta />
				<Links />
			</head>
			<body>
				<div
					className="fade-away absolute inset-0 bg-cover bg-top brightness-[0.3]"
					style={{ backgroundImage: `url(${background})` }}
				/>
				<div className="min-h-[100svh] flex flex-col relative">
					<AppHeader />
					<TooltipProvider delayDuration={500}>{children}</TooltipProvider>
					<AppFooter />
				</div>
				<ScrollRestoration />
				<LiveReload />
				<Scripts />
			</body>
		</html>
	)
}

export default ClerkApp(function App() {
	const { convexUrl } = useLoaderData<typeof loader>()
	const [convexClient] = useState(() => new ConvexReactClient(convexUrl))
	return (
		<ConvexProviderWithClerk client={convexClient} useAuth={useAuth}>
			<Document>
				<Outlet />
			</Document>
		</ConvexProviderWithClerk>
	)
})

export const ErrorBoundary = ClerkErrorBoundary()
