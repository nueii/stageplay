import {
	type HeadersFunction,
	type LoaderFunctionArgs,
	redirect,
} from "@remix-run/node"
import { db } from "~/db.server.ts"
import { raise } from "~/helpers/common.ts"
import { notFound } from "~/helpers/responses.ts"

export async function loader(args: LoaderFunctionArgs) {
	const characterId = args.params.characterId ?? raise("Missing characterId")

	const character = await db.character.findUnique({
		where: { id: characterId },
		select: { avatarUrl: true },
	})

	if (!character?.avatarUrl) {
		throw notFound()
	}

	return redirect(character.avatarUrl)
}

export const headers: HeadersFunction = () => ({
	"cache-control": "public, max-age=3600",
})
