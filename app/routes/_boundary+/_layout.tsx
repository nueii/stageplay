import { ClerkErrorBoundary } from "@clerk/remix"
import { Link, isRouteErrorResponse, useRouteError } from "@remix-run/react"
import { NotSignedInMessage } from "~/components/NotSignedInMessage.tsx"
import { Button } from "~/components/button.tsx"
import { ErrorLayout } from "~/components/error-layout"
import { toError } from "~/helpers/errors"
import { container } from "~/styles/common.ts"
import { NotFoundMessage } from "../../components/NotFoundMessage"

// rendering this boundary here to show errors while keeping the root layout
export const ErrorBoundary = ClerkErrorBoundary(function ErrorBoundary() {
	return (
		<main className={container()}>
			<ErrorBoundaryContent />
		</main>
	)
})

function ErrorBoundaryContent() {
	const error = useRouteError()

	if (!isRouteErrorResponse(error)) {
		const { message, stack } = toError(error)
		return (
			<ErrorLayout title="oops">
				<p>Something went wrong on our end.</p>
				<pre style={{ width: "100%", overflowX: "auto" }}>
					{stack || message}
				</pre>
				<Button outline asChild>
					<Link to="">Try again</Link>
				</Button>
			</ErrorLayout>
		)
	}

	if (error.status === 401) {
		return <NotSignedInMessage />
	}

	if (error.status === 403) {
		return (
			<ErrorLayout title="Not authorized">
				<p>You don&apos;t have permission to view this.</p>
			</ErrorLayout>
		)
	}

	if (error.status === 404) {
		return <NotFoundMessage />
	}

	return (
		<ErrorLayout title="oops">
			<p>Something went wrong on our end.</p>
			<Button outline asChild>
				<Link to="">Try again</Link>
			</Button>
		</ErrorLayout>
	)
}
