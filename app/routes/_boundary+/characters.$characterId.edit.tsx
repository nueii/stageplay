import {
	type ActionFunctionArgs,
	type LoaderFunctionArgs,
	type MetaFunction,
	json,
	redirect,
} from "@remix-run/node"
import { useActionData, useLoaderData } from "@remix-run/react"
import { Save } from "lucide-react"
import { requireUser } from "~/auth.server"
import { CharacterForm, characterFormSchema } from "~/components/character-form"
import { Panel } from "~/components/panel.tsx"
import { PendingFormButton } from "~/components/pending-form"
import { db } from "~/db.server"
import { raise } from "~/helpers/errors"
import { forbidden, notFound } from "~/helpers/responses"
import { getMeta } from "~/meta"
import { container, pageHeading } from "~/styles/common.ts"

export const meta: MetaFunction<typeof loader> = ({ data }) =>
	getMeta({ title: data ? `Editing ${data.character.name}` : undefined })

export async function loader({ request, params, ...args }: LoaderFunctionArgs) {
	await requireUser({ request, params, ...args })

	const character =
		(await db.character.findUnique({
			where: {
				id: params.characterId,
			},
			select: {
				id: true,
				name: true,
				avatarUrl: true,
				images: true,
				prefix: true,
				bio: true,
			},
		})) ?? raise(notFound("Character not found"))

	return json({ character })
}

export async function action({ request, params, ...args }: ActionFunctionArgs) {
	const user = await requireUser({ request, params, ...args })

	const character =
		(await db.character.findUnique({
			where: {
				id: params.characterId,
			},
			select: {
				id: true,
				user: {
					select: {
						discordId: true,
					},
				},
			},
		})) ?? raise(notFound("Character not found"))

	if (character.user.discordId !== user.discordId) {
		throw forbidden("You can only edit your own characters")
	}

	const result = characterFormSchema.safeParse(await request.formData())
	if (!result.success) {
		return json({ errors: result.error.formErrors })
	}

	await db.character.update({
		where: {
			id: character.id,
		},
		data: result.data,
	})

	return redirect(`/characters/${character.id}`)
}

export default function EditCharacterPage() {
	const { character } = useLoaderData<typeof loader>()
	const { errors } = useActionData<typeof action>() ?? {}
	return (
		<div className={container()}>
			<Panel border className="p-4 grid gap-4" asChild>
				<main>
					<h2 className={pageHeading()}>Editing {character.name}</h2>
					<CharacterForm character={character} errors={errors}>
						<PendingFormButton type="submit" className="button button-outline">
							<Save className="button-icon" /> Save
						</PendingFormButton>
					</CharacterForm>
				</main>
			</Panel>
		</div>
	)
}
