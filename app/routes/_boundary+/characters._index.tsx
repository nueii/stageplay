import { type LoaderFunctionArgs, json } from "@remix-run/node"
import { Link, useLoaderData, useNavigation } from "@remix-run/react"
import { VenetianMask } from "lucide-react"
import { useSpinDelay } from "spin-delay"
import { twMerge } from "tailwind-merge"
import { requireUser } from "~/auth.server"
import { Avatar } from "~/components/avatar"
import { Panel } from "~/components/panel"
import { RelativeTime } from "~/components/relative-time"
import { db } from "~/db.server"
import type { Nullish } from "~/helpers/types"
import { getMeta } from "~/meta"
import { container } from "~/styles/common.ts"

export const meta = () => getMeta({ title: "Your Characters" })

export async function loader({ request, ...args }: LoaderFunctionArgs) {
	const user = await requireUser({ request, ...args })

	const characters = await db.character
		.findMany({
			where: {
				user: {
					discordId: user.discordId,
				},
			},
			select: {
				id: true,
				name: true,
				avatarUrl: true,
				createdAt: true,
				lastPlayedAt: true,
			},
			orderBy: {
				createdAt: "desc",
			},
		})
		.then((characters) => {
			return characters.map((character) => ({
				...character,
				createdAt: character.createdAt.toISOString(),
				lastPlayedAt: character.lastPlayedAt?.toISOString(),
			}))
		})

	return json({ characters })
}

export default function CharacterListPage() {
	const { characters } = useLoaderData<typeof loader>()
	return characters.length > 0 ? (
		<main
			className={container(
				"max-w-screen-lg flex flex-wrap gap-4 justify-center",
			)}
		>
			{characters.map((character) => (
				<div key={character.id} className="w-full max-w-56">
					<CharacterCard character={character} />
				</div>
			))}
		</main>
	) : (
		<p>No characters yet!</p>
	)
}

function CharacterCard({
	character,
}: {
	character: {
		createdAt: string
		id: string
		name: string
		avatarUrl: string
		lastPlayedAt?: Nullish<string>
	}
}) {
	const navigation = useNavigation()

	const pending = useSpinDelay(
		navigation.state === "loading" &&
			navigation.location.pathname === `/characters/${character.id}`,
	)

	return (
		<Panel
			interactive
			border
			className={twMerge(
				"flex flex-col text-center",
				pending && "animate-pulse",
			)}
			asChild
		>
			<Link to={`/characters/${character.id}`} prefetch="intent">
				<section className="flex flex-col items-center gap-2 p-2">
					<h2 className="text-xl font-light leading-tight">{character.name}</h2>

					<Avatar
						src={character.avatarUrl}
						alt=""
						size={96}
						fallbackIcon={VenetianMask}
					/>
				</section>

				<footer className="grid flex-1 place-content-center bg-black/50 p-2 text-sm min-h-14 leading-tight opacity-75">
					<p>
						Created <RelativeTime date={character.createdAt} />
					</p>
					{!!character.lastPlayedAt && (
						<p>
							Last played <RelativeTime date={character.lastPlayedAt} />
						</p>
					)}
				</footer>
			</Link>
		</Panel>
	)
}
