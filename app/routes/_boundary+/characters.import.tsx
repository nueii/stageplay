import {
	type ActionFunctionArgs,
	type LoaderFunctionArgs,
	json,
	redirect,
} from "@remix-run/node"
import { useActionData, useSearchParams } from "@remix-run/react"
import { Import } from "lucide-react"
import { dedent } from "ts-dedent"
import { type ZodRawShape, z } from "zod"
import { zfd } from "zod-form-data"
import { requireUser } from "~/auth.server"
import { FormField } from "~/components/form-field"
import { Panel } from "~/components/panel.tsx"
import { PendingForm, PendingFormButton } from "~/components/pending-form"
import { db } from "~/db.server"
import { env } from "~/env.server"
import { bbcodeToMarkdown } from "~/helpers/convert-bbcode-to-markdown"
import { raise } from "~/helpers/errors"
import { getCharacterProfileUrl } from "~/helpers/f-list"
import { parseBBCode } from "~/helpers/parse-bbcode"
import { getMeta } from "~/meta"
import { container, pageHeading } from "~/styles/common.ts"

export const meta = () => getMeta({ title: "Import Character" })

export async function loader({ request, ...args }: LoaderFunctionArgs) {
	await requireUser({ request, ...args })
	return json({})
}

const actionBodySchema = zfd.formData({
	character: zfd.text(),
})

async function flistFetch(endpoint: string, body?: Record<string, string>) {
	const response = await fetch(new URL(endpoint, "https://www.f-list.net"), {
		method: "POST",
		headers: {
			"Content-Type": "application/x-www-form-urlencoded",
		},
		body: new URLSearchParams(body),
	})
	return response.json()
}

const flistResponseSchema = <T extends ZodRawShape>(schema: z.ZodObject<T>) =>
	schema.or(z.object({ error: z.string() }))

const ticketResponseSchema = flistResponseSchema(
	z.object({ ticket: z.string() }),
)

const infoTagSchema = z.object({
	id: z.string(),
	name: z.string(),
	type: z.string(),
})
type InfoTag = z.output<typeof infoTagSchema>

const listItemSchema = z.object({
	id: z.string(),
	name: z.string(),
	value: z.string(),
})
type ListItem = z.output<typeof listItemSchema>

const mappingDataResponseSchema = flistResponseSchema(
	z.object({
		infotags: z.array(infoTagSchema),
		listitems: z.array(listItemSchema),
	}),
)

const characterResponseSchema = flistResponseSchema(
	z.object({
		name: z.string(),
		description: z.string(),
		images: z.array(
			z.object({
				image_id: z.string(),
				extension: z.string(),
			}),
		),
		inlines: z.record(
			z.object({
				hash: z.string(),
				extension: z.string(),
			}),
		),
		infotags: z.record(z.string()),
	}),
)

const infoTagsByName = new Map<string, InfoTag>()
const listItems = new Map<string, ListItem>()

export async function action({ request, ...args }: ActionFunctionArgs) {
	const user = await requireUser({ request, ...args })
	const data = actionBodySchema.parse(await request.formData())

	const ticketResponse = ticketResponseSchema.parse(
		await flistFetch("/json/getApiTicket.php", {
			account: env.FLIST_ACCOUNT,
			password: env.FLIST_PASSWORD,
			no_characters: "true",
			no_friends: "true",
			no_bookmarks: "true",
		}),
	)
	if (!("ticket" in ticketResponse)) {
		console.error("Error fetching ticket:", ticketResponse.error)
		return json({ error: "An internal error occurred. Try again?" }, 500)
	}

	if (infoTagsByName.size === 0) {
		const mappingDataResponse = mappingDataResponseSchema.parse(
			await flistFetch("/json/api/mapping-list.php"),
		)
		if (!("infotags" in mappingDataResponse)) {
			console.error("Error fetching mapping data:", mappingDataResponse.error)
			return json({ error: "An internal error occurred. Try again?" }, 500)
		}

		for (const tag of mappingDataResponse.infotags) {
			infoTagsByName.set(tag.name, tag)
		}
		for (const item of mappingDataResponse.listitems) {
			listItems.set(item.id, item)
		}
	}

	const characterResponse = characterResponseSchema.parse(
		await flistFetch("/json/api/character-data.php", {
			account: env.FLIST_ACCOUNT,
			ticket: ticketResponse.ticket,
			name: data.character,
		}),
	)
	if (!("name" in characterResponse)) {
		const error = characterResponse.error || "Unknown error"
		console.error("Failed to import character:", error)
		return json({ error: `Failed to import character: ${error}` }, 400)
	}

	const avatarFileName = encodeURIComponent(
		characterResponse.name.toLowerCase(),
	)

	const profileUrl = getCharacterProfileUrl(characterResponse.name)

	const getInfoItem = (name: string) => {
		const tag = infoTagsByName.get(name)
		if (!tag) {
			console.warn(`Unknown info tag: ${name}`)
			return
		}

		const characterInfoTagValue = characterResponse.infotags[tag.id]
		if (!characterInfoTagValue) return

		if (tag.type === "list") {
			return listItems.get(characterInfoTagValue)?.value
		}

		return characterInfoTagValue
	}

	const infoItems = [
		"Age",
		"Apparent Age",
		"Gender",
		"Height/Length",
		"Species",
		"Occupation",
		"Orientation",
		"Personality",
	]
		.map((name) => {
			const value = getInfoItem(name)
			const label = name === "Height/Length" ? "Height" : name
			return value && `${label}: ${value}`
		})
		.filter(Boolean)
		.join("\n")

	const bio = dedent`
    ${infoItems}

    ---

    ${bbcodeToMarkdown(parseBBCode(characterResponse.description))}

    ---

    [View on F-List](${profileUrl})
  `

	const prefix =
		characterResponse.name.split(/\s+/)[0] ??
		raise("unexpected: prefix is undefined")

	const character = await db.character.create({
		data: {
			user: {
				connectOrCreate: {
					where: {
						discordId: user.discordId,
					},
					create: {
						discordId: user.discordId,
						name: user.name,
					},
				},
			},
			name: characterResponse.name,
			prefix: `${prefix.toLowerCase()}:`,
			bio,
			avatarUrl: `https://static.f-list.net/images/avatar/${avatarFileName}.png`,
			images: [
				...Object.values(characterResponse.inlines).map((inline) => {
					const part1 = inline.hash.slice(0, 2)
					const part2 = inline.hash.slice(2, 4)
					return `https://static.f-list.net/images/charinline/${part1}/${part2}/${inline.hash}.${inline.extension}`
				}),
				...characterResponse.images.map((image) => {
					return `https://static.f-list.net/images/charimage/${image.image_id}.${image.extension}`
				}),
			],
		},
		select: { id: true },
	})

	return redirect(`/characters/${character.id}/edit`)
}

export default function NewCharacterPage() {
	const data = useActionData<typeof action>()
	const [params] = useSearchParams()
	return (
		<div className={container()}>
			<Panel border asChild>
				<main className="p-4 grid gap-4">
					<h2 className={pageHeading()}>Import from F-List</h2>
					<PendingForm method="post" className="contents">
						{({ pending }) => (
							<>
								<fieldset disabled={pending}>
									<FormField label="Character Name or Profile URL">
										<input
											name="character"
											type="text"
											placeholder="Hitori Gotou or https://www.f-list.net/c/somethingsomething/"
											required
											className="input"
											defaultValue={params.get("character") ?? ""}
										/>
									</FormField>
									<PendingFormButton
										type="submit"
										className="button button-outline mt-4"
									>
										<Import className="button-icon" /> Import
									</PendingFormButton>
								</fieldset>
								{data?.error && !pending && <p role="alert">{data.error}</p>}
							</>
						)}
					</PendingForm>
				</main>
			</Panel>
		</div>
	)
}
