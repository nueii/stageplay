import {
	type ActionFunctionArgs,
	type LoaderFunctionArgs,
	json,
	redirect,
} from "@remix-run/node"
import { useActionData, useSearchParams } from "@remix-run/react"
import { Wand2 } from "lucide-react"
import { requireUser } from "~/auth.server"
import { CenteredPanelLayout } from "~/components/centered-panel-layout"
import { CharacterForm, characterFormSchema } from "~/components/character-form"
import { PendingFormButton } from "~/components/pending-form"
import { db } from "~/db.server"
import { getMeta } from "~/meta"

export const meta = () => getMeta({ title: "Create a Character" })

export async function loader({ request, ...args }: LoaderFunctionArgs) {
	await requireUser({ request, ...args })
	return json({})
}

export async function action({ request, ...args }: ActionFunctionArgs) {
	const user = await requireUser({ request, ...args })

	const result = characterFormSchema.safeParse(await request.formData())
	if (!result.success) {
		return json({ errors: result.error.formErrors })
	}

	const character = await db.character.create({
		data: {
			...result.data,
			images: { set: result.data.images },
			user: {
				connectOrCreate: {
					where: {
						discordId: user.discordId,
					},
					create: {
						discordId: user.discordId,
						name: user.name,
					},
				},
			},
		},
	})

	return redirect(`/characters/${character.id}`)
}

export default function NewCharacterPage() {
	const data = useActionData<typeof action>()
	const [params] = useSearchParams()
	return (
		<CenteredPanelLayout heading="New Character">
			<CharacterForm
				character={{ name: params.get("name") ?? "" }}
				errors={data?.errors}
			>
				<PendingFormButton type="submit" className="button button-outline">
					<Wand2 className="button-icon" /> Create
				</PendingFormButton>
			</CharacterForm>
		</CenteredPanelLayout>
	)
}
