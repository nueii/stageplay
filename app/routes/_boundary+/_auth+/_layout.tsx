import { Outlet } from "@remix-run/react"
import { useConvexAuth } from "convex/react"
import { EmptyState } from "~/components/EmptyState.tsx"
import { NotSignedInMessage } from "~/components/NotSignedInMessage.tsx"
import { Spinner } from "~/components/spinner.tsx"
import { container } from "~/styles/common.ts"

export default function AuthGuard() {
	const auth = useConvexAuth()

	if (auth.isLoading) {
		return (
			<main className={container()}>
				<EmptyState title="Just a moment...">
					<Spinner className="s-8" />
				</EmptyState>
			</main>
		)
	}

	if (!auth.isAuthenticated) {
		return (
			<main className={container()}>
				<NotSignedInMessage />
			</main>
		)
	}

	return <Outlet />
}
