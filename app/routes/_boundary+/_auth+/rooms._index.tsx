import type { MetaFunction } from "@remix-run/node"
import { Link } from "@remix-run/react"
import { api } from "convex-backend/_generated/api.js"
import { useConvexAuth, useQuery } from "convex/react"
import {
	LucideCrown,
	LucideMessageSquarePlus,
	LucideMessagesSquare,
} from "lucide-react"
import { EmptyState } from "~/components/EmptyState.tsx"
import { GridSkeleton } from "~/components/GridSkeleton.tsx"
import { ListGrid } from "~/components/ListGrid.tsx"
import { NotSignedInMessage } from "~/components/NotSignedInMessage.tsx"
import { PageSection } from "~/components/PageSection.tsx"
import { PageSectionSkeleton } from "~/components/PageSectionSkeleton.tsx"
import { Button } from "~/components/button.tsx"
import { Panel } from "~/components/panel.tsx"
import { getMeta } from "~/meta"
import { container } from "~/styles/common.ts"

export const meta: MetaFunction = () =>
	getMeta({
		title: "Rooms",
		description: "Browse chat rooms",
	})

export default function RoomListPage() {
	const auth = useConvexAuth()
	return (
		<main className={container("grid gap-6")}>
			{auth.isLoading ? (
				<PageSectionSkeleton />
			) : auth.isAuthenticated ? (
				<PageSection title="Your rooms">
					<RoomList />
				</PageSection>
			) : (
				<NotSignedInMessage />
			)}
		</main>
	)
}

function RoomList() {
	const rooms = useQuery(api.rooms.list, {})
	return rooms == null ? (
		<GridSkeleton />
	) : (
		<ListGrid
			items={[
				...rooms.ownedRooms.map((room) => ({
					...room,
					type: "owned" as const,
				})),
				...rooms.joinedRooms.map((room) => ({
					...room,
					type: "joined" as const,
				})),
			]}
			itemKey="_id"
			renderItem={(room) => (
				<Panel
					border
					interactive
					asChild
					className="flex items-center gap-2 p-2 text-xl/tight font-light"
				>
					<Link to={`/rooms/${room._id}`}>
						{room.type === "owned" ? (
							<LucideCrown aria-hidden />
						) : (
							<LucideMessagesSquare aria-hidden />
						)}
						<p className="flex-1">{room.title}</p>
					</Link>
				</Panel>
			)}
			emptyState={
				<EmptyState>
					<Button outline icon={LucideMessageSquarePlus} asChild>
						<Link to="/rooms/new">Create a room</Link>
					</Button>
				</EmptyState>
			}
		/>
	)
}
