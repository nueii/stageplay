import { Link, useParams } from "@remix-run/react"
import { api } from "convex-backend/_generated/api.js"
import type { Id } from "convex-backend/_generated/dataModel.js"
import { useMutation } from "convex/react"
import { useState } from "react"
import { Virtuoso } from "react-virtuoso"
import { z } from "zod"
import { ChatMessage, type ClientMessage } from "~/components/ChatMessage.tsx"
import { Chatbox } from "~/components/Chatbox.tsx"
import { EmptyState } from "~/components/EmptyState.tsx"
import { RoomSwitcher } from "~/components/RoomSwitcher.tsx"
import { Spinner } from "~/components/spinner.tsx"
import { useStableQuery } from "~/helpers/convex.ts"
import { isNonEmptyArray } from "~/helpers/non-empty-array.ts"
import { useCharactersFetcher } from "~/routes/api.characters/fetcher"

const paramsSchema = z.object({
	roomId: z.string().transform((id) => id as Id<"rooms">),
})

export default function RoomIndex() {
	const { roomId } = paramsSchema.parse(useParams())

	const [messages] = useStableQuery(api.messages.list, {
		roomId,
	})
	const createMessage = useMutation(api.messages.create)
	const charactersFetcher = useCharactersFetcher()
	const [optimisticMessages, setOptimisticMessages] = useState<ClientMessage[]>(
		[],
	)

	async function submitMessage(
		text: string,
		character: { id: string; name: string },
	) {
		const payload = {
			roomId,
			text: text,
			characterId: character.id,
			characterName: character.name,
		}

		const optimisticMessageKey = crypto.randomUUID() as Id<"messages">
		setOptimisticMessages((messages) => [
			...messages,
			{
				...payload,
				_id: optimisticMessageKey,
				_creationTime: Date.now(),
			},
		])

		try {
			await createMessage(payload)
			setOptimisticMessages((messages) => {
				return messages.filter((m) => m._id !== optimisticMessageKey)
			})
		} catch (error) {
			console.error(error)
			setOptimisticMessages((messages) => {
				return messages.map((m) => {
					if (m._id !== optimisticMessageKey) return m
					return {
						...m,
						failed: true,
					}
				})
			})
		}
	}

	function retrySubmitMessage(message: ClientMessage) {
		setOptimisticMessages((messages) => {
			return messages.filter((m) => m._id !== message._id)
		})
		submitMessage(message.text, {
			id: message.characterId,
			name: message.characterName,
		})
	}

	if (!charactersFetcher.data) {
		return (
			<EmptyState title="Just a moment...">
				<Spinner className="s-8" />
			</EmptyState>
		)
	}

	return (
		<>
			<div className="flex-1 min-h-0 flex divide-x divide-white/10">
				<section className="overflow-y-auto w-[200px] hidden md:block">
					<h3 className="sr-only">Rooms</h3>
					<RoomSwitcher />
				</section>
				<section className="flex-1 ring-accent-400/50 transition-shadow focus-within:ring-1">
					<h3 className="sr-only">Messages</h3>
					<Virtuoso<ClientMessage>
						style={{
							overflowY: "scroll",
						}}
						className="[&::-webkit-scrollbar-thumb]:bg-white/25 [&::-webkit-scrollbar-track]:bg-transparent [&::-webkit-scrollbar]:w-2"
						data={[...(messages ?? []), ...optimisticMessages]}
						computeItemKey={(_index, message) => message._id}
						itemContent={(_index, message) => (
							<ChatMessage
								message={message}
								onRetrySubmit={retrySubmitMessage}
							/>
						)}
						initialScrollTop={Infinity}
						followOutput
						alignToBottom
						overscan={10}
					/>
				</section>
			</div>

			{isNonEmptyArray(charactersFetcher.data.characters) ? (
				<Chatbox
					roomId={roomId}
					characters={charactersFetcher.data.characters}
					onSubmit={submitMessage}
				/>
			) : (
				<p className="px-3 py-2">
					<Link to="/characters/new" className="link">
						Create a character
					</Link>{" "}
					to join the conversation.
				</p>
			)}
		</>
	)
}
