import { usePush } from "@remix-pwa/push/client"
import { Outlet, useParams } from "@remix-run/react"
import { api } from "convex-backend/_generated/api.js"
import type { Id } from "convex-backend/_generated/dataModel.js"
import { useMutation } from "convex/react"
import { LucideBell, LucideMenu, LucideXOctagon } from "lucide-react"
import { useEffect, useState } from "react"
import { z } from "zod"
import { NotFoundMessage } from "~/components/NotFoundMessage.tsx"
import { RoomSwitcher } from "~/components/RoomSwitcher.tsx"
import { Button } from "~/components/button.tsx"
import { Panel } from "~/components/panel.tsx"
import { useStableQuery } from "~/helpers/convex.ts"
import { useRootLoaderData } from "~/root.data"
import { container } from "~/styles/common.ts"

const paramsSchema = z.object({
	roomId: z.string().transform((id) => id as Id<"rooms">),
})

export default function RoomLayout() {
	const { roomId } = paramsSchema.parse(useParams())
	const [room] = useStableQuery(api.rooms.get, { roomId })
	const createJoinedRoom = useMutation(api.joinedRooms.create)
	const [roomListOpen, setRoomListOpen] = useState(false)

	useEffect(() => {
		createJoinedRoom({ roomId })
	}, [createJoinedRoom, roomId])

	// close the room list when changing rooms
	// biome-ignore lint/correctness/useExhaustiveDependencies: this is intentional
	useEffect(() => {
		setRoomListOpen(false)
	}, [roomId])

	if (room === null) {
		return (
			<main className={container()}>
				<NotFoundMessage />
			</main>
		)
	}

	return (
		<main className={container("max-w-screen-lg")}>
			<Panel
				border
				className="flex h-[calc(100svh-10rem)] min-h-[24rem] flex-col divide-y divide-white/10 overflow-visible text-lg"
			>
				<header className="flex flex-row items-center gap-2 px-3 py-2 text-3xl font-light leading-tight h-12">
					<nav className="md:hidden">
						<Button
							type="button"
							onClick={() => setRoomListOpen((open) => !open)}
							className="-mx-2 px-2"
						>
							<LucideMenu className="s-6" />
							<span className="sr-only">Switch rooms</span>
						</Button>
					</nav>
					<h2 className="flex-1 flex items-center gap-2">{room?.title}</h2>
				</header>

				<NotificationPermissionsBanner />

				{roomListOpen ? (
					<section className="flex-1 min-h-0 overflow-y-auto">
						<h3 className="sr-only">Rooms</h3>
						<RoomSwitcher />
					</section>
				) : (
					<Outlet />
				)}
			</Panel>
		</main>
	)
}

function NotificationPermissionsBanner() {
	const [status, setStatus] = useState<NotificationPermission>()
	const [failed, setFailed] = useState(false)
	const rootData = useRootLoaderData()
	const { subscribeToPush } = usePush()

	useEffect(() => {
		setStatus(Notification.permission)
	}, [])

	const pushPublicKey = rootData?.pushPublicKey
	useEffect(() => {
		if (status === "granted" && pushPublicKey) {
			subscribeToPush(pushPublicKey, (subscription) => {
				fetch("/push/subscribe", {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
					},
					body: JSON.stringify(subscription),
				})
			})
		}
	}, [pushPublicKey, status, subscribeToPush])

	async function requestNotifications() {
		try {
			setStatus(await Notification.requestPermission())
			setFailed(false)
		} catch (error) {
			if (Notification.permission === "denied") {
				setStatus("denied")
				return
			}
			console.error(error)
			setFailed(true)
		}
	}

	if (failed) {
		return (
			<section className="p-3 flex flex-col gap-2">
				<h3 className="sr-only">Enable notifications</h3>
				<p>
					<LucideXOctagon className="s-5 inline-block align-text-top" />{" "}
					Something went wrong.
				</p>
				<div className="flex gap-2">
					<Button type="button" outline onClick={requestNotifications}>
						Try again
					</Button>
				</div>
			</section>
		)
	}

	if (status !== "default") {
		return null
	}

	return (
		<section className="p-3 flex flex-col gap-2">
			<h3 className="sr-only">Enable notifications</h3>
			<p>
				<LucideBell className="s-5 inline-block align-text-top" /> Would you
				like to enable notifications?
			</p>
			<div className="flex gap-2">
				<Button type="button" outline onClick={requestNotifications}>
					Manage notifications
				</Button>
			</div>
		</section>
	)
}
