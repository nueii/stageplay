import { useAuth } from "@clerk/remix"
import { getAuth } from "@clerk/remix/ssr.server"
import { type ActionFunctionArgs, redirect } from "@remix-run/node"
import {
	Form,
	useActionData,
	useLocation,
	useNavigation,
} from "@remix-run/react"
import { api } from "convex-backend/_generated/api.js"
import { ConvexHttpClient } from "convex/browser"
import { LucideInfo, LucideMessageSquarePlus } from "lucide-react"
import { z } from "zod"
import { ErrorList } from "~/components/ErrorList.tsx"
import { NotSignedInMessage } from "~/components/NotSignedInMessage.tsx"
import { Button } from "~/components/button.tsx"
import { CenteredPanelLayout } from "~/components/centered-panel-layout.tsx"
import { FormField } from "~/components/form-field.tsx"
import { env } from "~/env.server.ts"
import { container } from "~/styles/common.ts"

const schema = z.object({
	title: z
		.string()
		.min(1, { message: "This cannot be empty." })
		.max(256, { message: "Too long! The max is 256 characters." }),
})

export async function action({ request, ...args }: ActionFunctionArgs) {
	try {
		const parsed = schema.safeParse(
			Object.fromEntries(await request.formData()),
		)
		if (!parsed.success) {
			return {
				errors: parsed.error.formErrors,
			}
		}

		const auth = await getAuth({ request, ...args })
		const token = await auth.getToken({ template: "convex" })
		if (!token) {
			throw new Error("unexpected: unauthenticated")
		}

		const convex = new ConvexHttpClient(env.CONVEX_URL)
		convex.setAuth(token)

		const roomId = await convex.mutation(api.rooms.create, {
			title: parsed.data.title,
		})

		return redirect(`/rooms/${roomId}`)
	} catch (error) {
		console.error("Failed to create room.", error)
		return {
			errors: {
				formErrors: ["Something went wrong. Try again?"],
				fieldErrors: {},
			},
		}
	}
}

export default function NewRoomPage() {
	const navigation = useNavigation()
	const location = useLocation()
	const data = useActionData<typeof action>()
	const auth = useAuth()

	const pending =
		(navigation.state === "submitting" &&
			navigation.location.pathname === location.pathname) ||
		navigation.state === "loading"

	if (!auth.isSignedIn) {
		return (
			<main className={container()}>
				<NotSignedInMessage />
			</main>
		)
	}

	return (
		<CenteredPanelLayout heading="Create room">
			<aside>
				<LucideInfo className="inline-block s-5 align-text-top" /> Only those
				with the link to the room will be able to enter!
			</aside>
			<Form method="post" className="contents">
				<FormField label="Room title" errors={data?.errors.fieldErrors.title}>
					<input
						className="input"
						name="title"
						type="text"
						placeholder="The Comfiest Room"
					/>
				</FormField>

				<ErrorList errors={data?.errors.formErrors} />

				<Button
					type="submit"
					outline
					icon={LucideMessageSquarePlus}
					pending={pending}
					className="justify-self-start"
				>
					Create room
				</Button>
			</Form>
		</CenteredPanelLayout>
	)
}
