import { SignInButton, SignedIn, SignedOut } from "@clerk/remix"
import { Link } from "@remix-run/react"
import { LucideLogIn, Users } from "lucide-react"
import { AppLogo } from "~/components/app-logo"
import { Button } from "~/components/button"

export default function Hero() {
	return (
		<main className="flex flex-col flex-1 items-center justify-center px-4 py-32 text-center drop-shadow-md">
			<h2 className="mb-3">
				<div className="-mb-3 opacity-75">Welcome to</div>
				<div className="flex items-center gap-2">
					<AppLogo />
					<div className="flex items-center text-3xl font-light leading-none tracking-wide">
						Stageplay
					</div>
				</div>
			</h2>

			<p className="mb-6 max-w-xl text-xl font-light">
				Stageplay is a Discord app that makes it easier to put on the show of
				your dreams. Create characters and send messages as if you were really
				there!
			</p>

			<SignedIn>
				<Button outline icon={Users} asChild>
					<Link to="/characters">View your characters</Link>
				</Button>
			</SignedIn>
			<SignedOut>
				<SignInButton>
					<Button outline icon={LucideLogIn}>
						Sign in / register
					</Button>
				</SignInButton>
			</SignedOut>
		</main>
	)
}
