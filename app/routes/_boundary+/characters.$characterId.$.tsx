import * as Dialog from "@radix-ui/react-dialog"
import {
	type ActionFunctionArgs,
	type LoaderFunctionArgs,
	type MetaFunction,
	json,
	redirect,
} from "@remix-run/node"
import {
	useLoaderData,
	useNavigate,
	useNavigation,
	useParams,
} from "@remix-run/react"
import {
	ChevronLeft,
	ChevronRight,
	Edit,
	Trash,
	VenetianMask,
	X,
} from "lucide-react"
import { useEffect, useLayoutEffect, useState } from "react"
import { getUser, requireUser } from "~/auth.server"
import { Avatar } from "~/components/avatar"
import { BlurredBackgroundImage } from "~/components/blurred-background-image"
import ExternalLink from "~/components/external-link.tsx"
import { Panel } from "~/components/panel.tsx"
import { PendingForm, PendingFormButton } from "~/components/pending-form"
import { PendingLink } from "~/components/pending-link"
import { RelativeTime } from "~/components/relative-time"
import { db } from "~/db.server"
import { raise } from "~/helpers/errors"
import { renderMarkdown } from "~/helpers/markdown.server.ts"
import { pick } from "~/helpers/pick"
import { range } from "~/helpers/range"
import { forbidden, notFound } from "~/helpers/responses"
import { wrappedMod } from "~/helpers/wrapped-mod"
import { getMeta } from "~/meta"
import { container } from "~/styles/common.ts"

export const meta: MetaFunction<typeof loader> = ({ data }) => {
	if (!data?.character) return getMeta()
	return getMeta({
		title: data.character.name,
		description: `View ${data.character.name}'s profile on Stageplay`,
		image: data.character.images[0],
	})
}

export async function loader({ request, params, ...args }: LoaderFunctionArgs) {
	const user = await getUser({ request, params, ...args })

	const character =
		(await db.character.findUnique({
			where: {
				id: params.characterId,
			},
			select: {
				id: true,
				name: true,
				avatarUrl: true,
				images: true,
				createdAt: true,
				bio: true,
				user: {
					select: {
						discordId: true,
					},
				},
			},
		})) ?? raise(notFound("Character not found"))

	return json({
		isOwner: character.user.discordId === user?.discordId,
		character: {
			...pick(character, ["id", "createdAt", "name", "avatarUrl", "images"]),
			bio: renderMarkdown(character.bio),
		},
	})
}

export async function action({ request, params, ...args }: ActionFunctionArgs) {
	if (request.method === "DELETE") {
		const user = await requireUser({ request, params, ...args })

		const character =
			(await db.character.findUnique({
				where: {
					id: params.characterId,
				},
				select: {
					id: true,
					user: {
						select: {
							discordId: true,
						},
					},
				},
			})) ?? raise(notFound("Character not found"))

		if (character.user.discordId !== user.discordId) {
			throw forbidden("You can only delete your own characters")
		}

		await db.character.delete({
			where: {
				id: character.id,
			},
		})

		return redirect("/characters")
	}

	return new Response(undefined, { status: 405 })
}

export default function CharacterPage() {
	const { character, isOwner } = useLoaderData<typeof loader>()
	return (
		<div className={container()}>
			<Panel border>
				<header className="flex flex-col items-center gap-3 border-b border-white/10 bg-black/50 p-4 text-center">
					<Avatar
						src={character.avatarUrl}
						alt=""
						size={96}
						fallbackIcon={VenetianMask}
					/>

					<h2 className="text-2xl font-light">{character.name}</h2>
					<p className="opacity-75">
						Created <RelativeTime date={character.createdAt} />
					</p>

					{isOwner && (
						<nav className="flex gap-2">
							<PendingLink
								to={`/characters/${character.id}/edit`}
								prefetch="intent"
								className="button button-outline"
							>
								<Edit className="button-icon" />
								Edit
							</PendingLink>
							<DeleteButton character={character} />
						</nav>
					)}
				</header>

				<main>
					<CharacterImageCarousel images={character.images} />
					<hr className="border-white/10" />
					{/* biome-ignore lint/security/noDangerouslySetInnerHtml: <explanation> */}
					<div className="prose m-4" dangerouslySetInnerHTML={character.bio} />
				</main>
			</Panel>
		</div>
	)
}

function CharacterImageCarousel({ images }: { images: string[] }) {
	const [index, setIndex] = useState(0)
	const overscan = 5

	if (images.length === 0) return null

	return (
		<div className="relative aspect-square sm:aspect-video">
			{range(index - overscan, index + overscan + 1).map((i) => (
				<ExternalLink
					key={i}
					href={images[wrappedMod(i, images.length)]}
					title={`View image ${i + 1} in new tab`}
					className="absolute inset-0 transition-transform duration-300 motion-reduce:transition-none"
					style={{
						transform: `translateX(${(i - index) * 100}%)`,
					}}
				>
					<BlurredBackgroundImage
						src={images[wrappedMod(i, images.length)] ?? ""}
					/>
				</ExternalLink>
			))}
			<button
				type="button"
				className="panel panel-interactive absolute left-2 top-1/2 -translate-y-1/2 p-2"
				title="Show previous image"
				onClick={() => setIndex((i) => i - 1)}
			>
				<ChevronLeft />
			</button>
			<button
				type="button"
				className="panel panel-interactive absolute right-2 top-1/2 -translate-y-1/2 p-2"
				title="Show previous image"
				onClick={() => setIndex((i) => i + 1)}
			>
				<ChevronRight />
			</button>
			<div className="absolute inset-x-2 bottom-0 flex flex-wrap justify-center overflow-x-auto">
				{range(
					floorToNearest(index, images.length),
					floorToNearest(index, images.length) + images.length,
				).map((i) => (
					<button
						type="button"
						key={i}
						className="p-1.5"
						onClick={() => setIndex(i)}
						title={`Show image ${i + 1}`}
					>
						<div
							className="rounded-full border border-white/10 bg-white/50 s-5 data-[current]:bg-accent-500"
							data-current={i === index || undefined}
						/>
					</button>
				))}
			</div>
		</div>
	)
}

function floorToNearest(n: number, multiple: number) {
	return Math.floor(n / multiple) * multiple
}

function DeleteButton({
	character,
}: {
	character: {
		id: string
		name: string
	}
}) {
	const mounted = useMounted()
	const params = useParams()
	const navigation = useNavigation()
	const navigate = useNavigate()

	const open = mounted && params["*"]?.startsWith("delete")
	const pending = navigation.state !== "idle"

	return (
		<Dialog.Root
			open={open}
			onOpenChange={(open) => !open && navigate(`/characters/${character.id}`)}
		>
			<Dialog.Trigger asChild>
				<PendingLink
					className="button button-outline"
					to={`/characters/${character.id}/delete`}
				>
					<Trash className="button-icon" /> Delete
				</PendingLink>
			</Dialog.Trigger>
			<Dialog.Portal>
				<Dialog.Overlay className="fixed inset-0 flex flex-col overflow-y-auto bg-black/50 backdrop-blur animate-in animate-from-opacity-0 animate-duration-300 data-[state=closed]:animate-out">
					<Dialog.Content className="panel m-auto flex max-w-md flex-col gap-3 p-4 text-center animate-in animate-from-scale-95 animate-duration-300 data-[state=closed]:animate-out">
						<Dialog.Title className="text-2xl font-light">
							Delete Character
						</Dialog.Title>
						<Dialog.Description>
							Are you sure you want to delete <strong>{character.name}</strong>?
							This action cannot be undone.
						</Dialog.Description>
						<div className="flex justify-center gap-2">
							<Dialog.Close asChild>
								<PendingLink
									to={`/characters/${character.id}`}
									className="button button-outline"
									data-disabled={pending ? "true" : undefined}
								>
									<X className="button-icon" /> Cancel
								</PendingLink>
							</Dialog.Close>
							<PendingForm method="delete">
								<PendingFormButton
									type="submit"
									className="button button-outline"
									disabled={pending}
								>
									<Trash className="button-icon" /> Delete
								</PendingFormButton>
							</PendingForm>
						</div>
					</Dialog.Content>
				</Dialog.Overlay>
			</Dialog.Portal>
		</Dialog.Root>
	)
}

function useMounted() {
	const [mounted, setMounted] = useState(false)
	useIsomorphicLayoutEffect(() => {
		setMounted(true)
	}, [])
	return mounted
}

const useIsomorphicLayoutEffect =
	typeof window === "undefined" ? useEffect : useLayoutEffect
