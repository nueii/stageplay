import { useFetcher } from "@remix-run/react"
import { useEffect } from "react"
import type { loader } from "~/routes/api.characters/route"

export function useCharactersFetcher() {
	const { load, ...fetcher } = useFetcher<typeof loader>()

	useEffect(() => {
		load("/api/characters")
	}, [load])

	return { load, ...fetcher }
}
