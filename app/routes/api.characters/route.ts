import type { LoaderFunctionArgs } from "@remix-run/node"
import { requireUser } from "~/auth.server.ts"
import { db } from "~/db.server.ts"

export async function loader(args: LoaderFunctionArgs) {
	const user = await requireUser(args)

	const characters = await db.character.findMany({
		where: {
			user: {
				discordId: user.discordId,
			},
		},
		select: {
			id: true,
			name: true,
		},
	})

	return {
		characters,
	}
}
