import { getAuth } from "@clerk/remix/ssr.server"
import type { ActionFunctionArgs } from "@remix-run/node"
import { json } from "@remix-run/node"
import { api } from "convex-backend/_generated/api.js"
import { ConvexHttpClient } from "convex/browser"
import { ZodError, z } from "zod"
import { env } from "~/env.server.ts"
import { raise } from "~/helpers/common.ts"
import { unauthorized } from "~/helpers/responses.ts"

const bodySchema = z.object({
	subscription: z.object({
		endpoint: z.string(),
		keys: z.object({
			p256dh: z.string(),
			auth: z.string(),
		}),
	}),
})

export async function action(args: ActionFunctionArgs) {
	try {
		const auth = await getAuth(args)
		const token =
			(await auth.getToken({ template: "convex" })) ?? raise(unauthorized())

		const body = bodySchema.parse(await args.request.clone().json())

		const client = new ConvexHttpClient(env.CONVEX_URL)
		client.setAuth(token)
		await client.mutation(api.pushSubscriptions.create, body.subscription)

		return json({ ok: true })
	} catch (error) {
		if (error instanceof ZodError) {
			const bodyText = await args.request
				.clone()
				.text()
				.catch(() => "N/A")

			const message = [
				"[/push/subscribe] Parsing error.",
				`body: ${bodyText}`,
				`errors: ${JSON.stringify(error.format(), null, 2)}`,
			]
			throw new Error(message.join("\n"))
		}

		throw error
	}
}
