import { api } from "convex-backend/_generated/api.js"
import type { Id } from "convex-backend/_generated/dataModel.js"
import { useMutation, useQuery } from "convex/react"
import { LucideSendHorizontal } from "lucide-react"
import { useMemo, useRef } from "react"
import TextArea from "react-textarea-autosize"
import { CharacterSwitcher } from "~/components/CharacterSwitcher.tsx"
import { Button } from "~/components/button.tsx"
import type { NonEmptyArray } from "~/helpers/non-empty-array.ts"
import { parseTruthy } from "~/helpers/parseTruthy.ts"
import { useDebouncedCallback } from "~/helpers/useDebouncedCallback.ts"

export function Chatbox({
	roomId,
	characters,
	onSubmit,
}: {
	roomId: Id<"rooms">
	characters: NonEmptyArray<{ id: string; name: string }>
	onSubmit: (text: string, character: { id: string; name: string }) => void
}) {
	const roomConfig = useQuery(api.roomConfigs.get, { roomId })
	const updateRoomConfig = useDebouncedCallback(
		useMutation(api.roomConfigs.update),
		1000,
	)
	const chatboxRef = useRef<HTMLTextAreaElement>(null)

	const currentCharacter =
		characters.find((c) => c.id === roomConfig?.character?.id) ?? characters[0]

	const sortedCharacters = useMemo(() => {
		return characters.toSorted((a, b) => a.name.localeCompare(b.name))
	}, [characters])

	function handleSubmit(rawText: string) {
		const text = rawText.trim()
		if (!text) return

		onSubmit(text, currentCharacter)
		parseTruthy(chatboxRef.current).value = ""
		updateRoomConfig({
			roomId,
			message: "",
		})
	}

	return (
		<section
			className="flex items-stretch divide-x divide-white/10 rounded-b-md [&>:first-child]:rounded-bl-md [&>:last-child]:rounded-br-md"
			aria-label="Chatbox"
		>
			<CharacterSwitcher
				characters={sortedCharacters}
				currentCharacter={currentCharacter}
				onSelectCharacter={(newCharacter) => {
					updateRoomConfig({
						roomId,
						character: newCharacter,
					})
				}}
			/>

			<input type="hidden" name="characterName" value={currentCharacter.name} />

			<TextArea
				placeholder={`Say something as ${currentCharacter.name}`}
				className="flex-1 resize-none bg-transparent px-3 py-2 leading-6 text-inherit ring-accent-400/50 transition-shadow focus:outline-none focus:ring-1"
				aria-label="Message"
				name="text"
				minRows={2}
				maxRows={8}
				defaultValue={roomConfig?.message ?? ""}
				onChange={(event) => {
					updateRoomConfig({
						roomId,
						message: event.target.value,
					})
				}}
				onKeyDown={(event) => {
					if (event.key === "Enter" && !event.shiftKey && !event.ctrlKey) {
						event.preventDefault()
						handleSubmit(event.currentTarget.value)
					}
				}}
				ref={chatboxRef}
			/>

			<Button
				type="button"
				icon={LucideSendHorizontal}
				className="h-auto rounded-none ring-accent-400/50 transition-shadow focus:outline-none focus:ring-1"
				onClick={() => {
					handleSubmit(parseTruthy(chatboxRef.current).value)
				}}
			>
				<span className="sr-only sm:not-sr-only">Send</span>
			</Button>
		</section>
	)
}
