import { SignInButton, SignOutButton, useUser } from "@clerk/remix"
import { Link } from "@remix-run/react"
import {
	Import,
	LogOut,
	LucideChevronDown,
	LucideMessageSquarePlus,
	LucideMessagesSquare,
	User,
	UserPlus,
	Users,
} from "lucide-react"
import { useRootLoaderData } from "~/root.data"
import { AppLogo } from "./app-logo"
import { Avatar } from "./avatar"
import { Button } from "./button"
import ExternalLink from "./external-link.tsx"
import {
	HeaderMenu,
	HeaderMenuContent,
	HeaderMenuItem,
	HeaderMenuTrigger,
} from "./header-menu"
import { Panel } from "./panel"

export function AppHeader() {
	const { user } = useUser()
	const rootData = useRootLoaderData()

	return (
		<Panel border="bottom" asChild>
			<header
				className="flex items-center gap-2 px-4 h-12"
				aria-label="Main header"
			>
				<Link to="/">
					<AppLogo small />
					<span className="sr-only">Home</span>
				</Link>

				<div className="flex flex-1 items-center justify-center sm:justify-end">
					{user && (
						<>
							<HeaderMenu>
								<HeaderMenuTrigger asChild>
									<Button icon={LucideChevronDown} iconPosition="end">
										Characters
									</Button>
								</HeaderMenuTrigger>
								<HeaderMenuContent>
									<HeaderMenuItem icon={Users} asChild>
										<Link to="/characters">My Characters</Link>
									</HeaderMenuItem>
									<HeaderMenuItem icon={UserPlus} asChild>
										<Link to="/characters/new">New Character</Link>
									</HeaderMenuItem>
									<HeaderMenuItem icon={Import} asChild>
										<Link to="/characters/import">Import from F-List</Link>
									</HeaderMenuItem>
								</HeaderMenuContent>
							</HeaderMenu>

							<HeaderMenu>
								<HeaderMenuTrigger asChild>
									<Button icon={LucideChevronDown} iconPosition="end">
										Chat
									</Button>
								</HeaderMenuTrigger>
								<HeaderMenuContent>
									<HeaderMenuItem icon={LucideMessagesSquare} asChild>
										<Link to="/rooms">Browse Rooms</Link>
									</HeaderMenuItem>
									<HeaderMenuItem icon={LucideMessageSquarePlus} asChild>
										<Link to="/rooms/new">Create Room</Link>
									</HeaderMenuItem>
								</HeaderMenuContent>
							</HeaderMenu>
						</>
					)}
				</div>

				{user ? (
					<HeaderMenu>
						<HeaderMenuTrigger className="transition hover:opacity-75" asChild>
							<button type="button">
								<Avatar
									src={user.imageUrl}
									alt=""
									size={32}
									fallbackIcon={User}
								/>
								<span className="sr-only">My Account</span>
							</button>
						</HeaderMenuTrigger>
						<HeaderMenuContent align="end">
							<p className="flex-1 px-3 py-2 leading-none">
								<span className="mb-0.5 inline-block text-sm opacity-75">
									logged in as
								</span>
								<br />
								<span>{user.username}</span>
							</p>
							{rootData?.clerkAccountPortalUrl && (
								<HeaderMenuItem icon={User} asChild>
									<ExternalLink href={rootData.clerkAccountPortalUrl}>
										Manage Account
									</ExternalLink>
								</HeaderMenuItem>
							)}
							<SignOutButton>
								<HeaderMenuItem icon={LogOut}>Log out</HeaderMenuItem>
							</SignOutButton>
						</HeaderMenuContent>
					</HeaderMenu>
				) : (
					<SignInButton>
						<Button>Sign in / register</Button>
					</SignInButton>
				)}
			</header>
		</Panel>
	)
}
