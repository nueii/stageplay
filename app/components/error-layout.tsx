import type { ReactNode } from "react"

export function ErrorLayout({
	title,
	children,
}: {
	title: ReactNode
	children: ReactNode
}) {
	return (
		<section>
			<h2 className="text-4xl font-light mb-2">{title}</h2>
			<div className="flex flex-col items-start gap-4">{children}</div>
		</section>
	)
}
