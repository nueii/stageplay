export function EmptyState({
	title = "Nothing here!",
	children,
}: {
	title?: React.ReactNode
	children?: React.ReactNode
}) {
	return (
		<section className="flex flex-col items-center justify-center gap-3 py-8 px-4">
			<h3 className="text-2xl font-light opacity-75">{title}</h3>
			{children}
		</section>
	)
}
