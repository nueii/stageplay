import { NavLink, useNavigate } from "@remix-run/react"
import { api } from "convex-backend/_generated/api.js"
import type { Doc } from "convex-backend/_generated/dataModel.js"
import { useMutation, useQuery } from "convex/react"
import { LucideCrown, LucideMessageSquare, LucideX } from "lucide-react"
import { Button, type ButtonIcon } from "~/components/button.tsx"
import { Spinner } from "~/components/spinner.tsx"
import { Tooltip, TooltipContent, TooltipTrigger } from "./Tooltip.tsx"

export function RoomSwitcher() {
	const list = useQuery(api.rooms.list, {})
	const navigate = useNavigate()

	return list === undefined ? (
		<Spinner className="s-8" />
	) : (
		<ul>
			{list.ownedRooms.map((room) => (
				<RoomSwitcherLink key={room._id} room={room} icon={LucideCrown} />
			))}
			{list.joinedRooms.map((room) => (
				<LeaveButtonWrapper
					key={room._id}
					room={room}
					onLeave={() => {
						const firstRoom = list.ownedRooms[0] || list.joinedRooms[0]
						if (firstRoom) {
							navigate(`/rooms/${firstRoom._id}`, { replace: true })
						} else {
							navigate("/rooms", { replace: true })
						}
					}}
				>
					<RoomSwitcherLink room={room} icon={LucideMessageSquare} />
				</LeaveButtonWrapper>
			))}
		</ul>
	)
}

function RoomSwitcherLink({
	room,
	icon,
}: {
	room: Doc<"rooms">
	icon: ButtonIcon
}) {
	return (
		<Button
			icon={icon}
			className="rounded-none w-full justify-start text-left"
			asChild
		>
			<NavLink to={`/rooms/${room._id}`}>
				<span className="line-clamp-2">{room.title}</span>
			</NavLink>
		</Button>
	)
}

function LeaveButtonWrapper({
	room,
	onLeave,
	children,
}: {
	room: Doc<"rooms">
	onLeave: () => void
	children: React.ReactNode
}) {
	const removeJoinedRoom = useMutation(
		api.joinedRooms.remove,
	).withOptimisticUpdate((store, args) => {
		const rooms = store.getQuery(api.rooms.list, {})
		if (!rooms) return

		store.setQuery(
			api.rooms.list,
			{},
			{
				...rooms,
				joinedRooms: rooms.joinedRooms.filter(
					(room) => room._id !== args.roomId,
				),
			},
		)
	})

	return (
		<div className="relative group">
			{children}
			<div className="absolute inset-y-0 flex items-center right-0">
				<Tooltip disableHoverableContent>
					<TooltipTrigger asChild>
						<button
							type="button"
							className="h-full px-2 hover:!opacity-100 group-hover:opacity-75 focus-visible:opacity-75 opacity-0 transition-opacity"
							onClick={() => {
								onLeave()
								removeJoinedRoom({ roomId: room._id })
							}}
						>
							<LucideX className="s-5" />
							<span className="sr-only">Leave {room.title}</span>
						</button>
					</TooltipTrigger>
					<TooltipContent>Leave</TooltipContent>
				</Tooltip>
			</div>
		</div>
	)
}
