import { SignInButton } from "@clerk/remix"
import { ErrorLayout } from "~/components/error-layout"

export function NotSignedInMessage() {
	return (
		<ErrorLayout title="Not signed in">
			<p>
				Please{" "}
				<SignInButton>
					<button type="button" className="link">
						sign in or register
					</button>
				</SignInButton>{" "}
				to continue.
			</p>
		</ErrorLayout>
	)
}
