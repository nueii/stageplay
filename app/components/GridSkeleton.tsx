import { range } from "~/helpers/range.ts"
import { skeletonBlock } from "~/styles/common.ts"
import { EmptyState } from "./EmptyState"
import { ListGrid } from "./ListGrid"

export function GridSkeleton() {
	return (
		<ListGrid
			items={range(6).map((_, index) => ({ key: index }))}
			itemKey="key"
			renderItem={() => <div className={skeletonBlock("h-24 rounded-md")} />}
			emptyState={<EmptyState />}
		/>
	)
}
