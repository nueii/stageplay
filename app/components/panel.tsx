import { Slot } from "@radix-ui/react-slot"
import type { ComponentPropsWithRef } from "react"
import { twMerge } from "tailwind-merge"
import { autoRef } from "~/helpers/auto-ref"

export interface PanelProps extends ComponentPropsWithRef<"div"> {
	asChild?: boolean
	border?: boolean | "top" | "bottom" | "left" | "right" | "x" | "y"
	interactive?: boolean
}

export const Panel = autoRef(function Panel({
	asChild,
	border,
	interactive,
	...props
}: PanelProps) {
	const Component = asChild ? Slot : "div"
	return (
		<Component
			{...props}
			className={twMerge(
				"block border-white/10 bg-black/60 shadow-md shadow-black/40 backdrop-blur",

				border === true && "overflow-clip rounded-md border",
				border === "top" && "border-t",
				border === "bottom" && "border-b",
				border === "left" && "border-l",
				border === "right" && "border-r",
				border === "x" && "border-x",
				border === "y" && "border-y",

				interactive &&
					"active-press transition hover:border-accent-300/30 hover:text-accent-300",

				props.className,
			)}
		/>
	)
})
