import type { CSSProperties } from "react"
import type { Nullish } from "~/helpers/types"

export type AvatarFallbackIconProps = {
	style?: CSSProperties
}

export function Avatar(props: {
	src: Nullish<string>
	alt: string
	size: number
	fallbackIcon: React.ComponentType<AvatarFallbackIconProps>
}) {
	const src = props.src?.trim()
	return src ? (
		<img
			src={src}
			alt={props.alt}
			className="rounded-full"
			style={{ width: props.size, height: props.size }}
		/>
	) : (
		<div
			className="flex items-center justify-center rounded-full border border-white/10 bg-black/50 s-20"
			style={{ width: props.size, height: props.size }}
		>
			<props.fallbackIcon
				style={{ width: props.size * 0.6, height: props.size * 0.6 }}
				aria-hidden
			/>
		</div>
	)
}
