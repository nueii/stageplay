import { type ReactNode, useId } from "react"
import { type Slot, renderSlot } from "../helpers/slot.js"
import { ErrorList } from "./ErrorList.js"

export function FormField(props: {
	label: ReactNode
	description?: ReactNode
	errors?: string[]
	children: Slot<{ id: string; "aria-describedby": string | undefined }>
}) {
	const id = useId()
	const descriptionId = useId()
	return (
		<div>
			<label htmlFor={id}>
				<div className="leading-5">{props.label}</div>
			</label>
			{props.description != null && (
				<p id={descriptionId} className="mb-1 text-sm leading-tight opacity-75">
					{props.description}
				</p>
			)}

			{renderSlot(props.children, {
				id,
				"aria-describedby":
					props.description == null ? undefined : descriptionId,
			})}

			<ErrorList errors={props.errors} />
		</div>
	)
}
