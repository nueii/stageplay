import type { ReactNode } from "react"
import { useSpinDelay } from "spin-delay"

export function SpinDelay(props: {
	loading: boolean
	children: (props: { loading: boolean }) => ReactNode
}) {
	const loading = useSpinDelay(props.loading)
	return <>{props.children({ loading })}</>
}
