import { Slot, Slottable } from "@radix-ui/react-slot"
import type { ComponentPropsWithRef, ComponentType } from "react"
import { useSpinDelay } from "spin-delay"
import { twMerge } from "tailwind-merge"
import { autoRef } from "~/helpers/auto-ref"
import { Spinner } from "./spinner"

export interface ButtonProps extends ComponentPropsWithRef<"button"> {
	asChild?: boolean
	outline?: boolean
	active?: boolean
	icon?: ButtonIcon
	iconPosition?: "start" | "end"
	pending?: boolean
}

export type ButtonIcon = ComponentType<{
	className?: string
}>

export const Button = autoRef(function Button({
	asChild,
	outline,
	active,
	icon,
	iconPosition = "start",
	pending: pendingProp,
	children,
	...props
}: ButtonProps) {
	const Component = asChild ? Slot : "button"
	const pending = useSpinDelay(pendingProp ?? false)

	let IconComponent
	if (icon && pending) {
		IconComponent = Spinner
	} else if (icon) {
		IconComponent = icon
	}

	return (
		<Component
			{...props}
			disabled={props.disabled || pendingProp}
			className={twMerge(
				"active-press inline-flex h-10 items-center justify-center gap-2 rounded-md px-3 font-medium leading-none transition hover:bg-white/10 [&.active]:text-accent-300",
				outline && "border border-white/10",
				active && "text-accent-300",
				props.className,
			)}
		>
			{IconComponent && iconPosition === "start" && (
				<IconComponent className="-ml-0.5 block s-5 flex-shrink-0" />
			)}
			<Slottable>{children}</Slottable>
			{IconComponent && iconPosition === "end" && (
				<IconComponent className="-mr-0.5 block s-5 flex-shrink-0" />
			)}
		</Component>
	)
})
