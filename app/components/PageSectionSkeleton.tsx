import { skeletonBlock } from "~/styles/common.ts"
import { GridSkeleton } from "./GridSkeleton.tsx"
import { PageSection } from "./PageSection.tsx"

export function PageSectionSkeleton() {
	return (
		<PageSection
			title={
				<div
					className={skeletonBlock(
						"h-[1em] w-full max-w-48 inline-block rounded-sm",
					)}
				/>
			}
		>
			<GridSkeleton />
		</PageSection>
	)
}
