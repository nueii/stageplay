import { pageHeading } from "~/styles/common.ts"

export function PageSection({
	title,
	children,
}: {
	title: React.ReactNode
	children: React.ReactNode
}) {
	return (
		<section>
			<h2 className={pageHeading("mb-2")}>{title}</h2>
			{children}
		</section>
	)
}
