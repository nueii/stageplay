import { NavLink, type NavLinkProps } from "@remix-run/react"
import { type ForwardedRef, type ReactNode, forwardRef } from "react"
import { SpinDelay } from "~/components/spin-delay"
import { Spinner } from "~/components/spinner"
import type { Overwrite } from "~/helpers/types"

type PendingLinkProps = Overwrite<
	NavLinkProps,
	{
		children: ReactNode | ((props: { pending: boolean }) => ReactNode)
		pending?: ReactNode
	}
>

export const PendingLink = forwardRef(function PendingLink(
	{
		children,
		pending: pendingChildren = (
			<>
				<Spinner className="button-icon" /> Please wait...
			</>
		),
		...props
	}: PendingLinkProps,
	ref: ForwardedRef<HTMLAnchorElement>,
) {
	return (
		<NavLink {...props} ref={ref}>
			{({ isPending }) => (
				<SpinDelay loading={isPending}>
					{({ loading }) => {
						if (typeof children === "function") {
							return children({ pending: loading })
						}
						if (loading) {
							return pendingChildren
						}
						return children
					}}
				</SpinDelay>
			)}
		</NavLink>
	)
})
