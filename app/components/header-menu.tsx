import * as Popover from "@radix-ui/react-popover"
import { twMerge } from "tailwind-merge"
import { autoRef } from "~/helpers/auto-ref"
import { Button, type ButtonProps } from "./button"
import { Panel } from "./panel"

export function HeaderMenu(props: Popover.PopoverProps) {
	return <Popover.Root {...props} />
}

export const HeaderMenuTrigger = Popover.Trigger

export function HeaderMenuContent(props: Popover.PopoverContentProps) {
	return (
		<Popover.Portal>
			<Panel
				border
				className={twMerge(
					"flex min-w-48 flex-col animate-from-opacity-0 animate-from-translate-y-1 data-[state=open]:animate-in data-[state=closed]:animate-out",
					props.className,
				)}
				asChild
			>
				<Popover.Content sideOffset={8} collisionPadding={8} {...props} />
			</Panel>
		</Popover.Portal>
	)
}

export const HeaderMenuItem = autoRef(function HeaderMenuItem(
	props: ButtonProps,
) {
	return (
		<Popover.Close asChild>
			<Button
				{...props}
				className={twMerge("justify-start rounded-none", props.className)}
			/>
		</Popover.Close>
	)
})
