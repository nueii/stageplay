import ExternalLink from "./external-link"

export function BlurredBackgroundImage({ src }: { src: string }) {
	return (
		<div className="relative block overflow-clip s-full">
			<img
				src={src}
				alt=""
				className="absolute inset-0 h-full w-full scale-110 object-cover blur-md brightness-50"
			/>
			<img
				src={src}
				alt=""
				className="absolute inset-0 h-full w-full object-contain drop-shadow-lg"
			/>
		</div>
	)
}

export function BlurredBackgroundImageLink({ src }: { src: string }) {
	return (
		<ExternalLink title="View image in new tab" href={src}>
			<BlurredBackgroundImage src={src} />
		</ExternalLink>
	)
}
