import { Slot } from "@radix-ui/react-slot"
import { twMerge } from "tailwind-merge"
import { autoRef } from "~/helpers/auto-ref.tsx"

export interface ContainerProps extends React.ComponentPropsWithRef<"div"> {
	asChild?: boolean
	size?: "screen-sm" | "screen-md" | "screen-lg"
}

export const Container = autoRef(function Container({
	asChild,
	size,
	className,
	...props
}: ContainerProps) {
	const sizeClass = {
		"screen-sm": twMerge("max-w-screen-sm"),
		"screen-md": twMerge("max-w-screen-md"),
		"screen-lg": twMerge("max-w-screen-lg"),
	}[size ?? "screen-md"]

	const Component = asChild ? Slot : "div"
	return (
		<Component
			{...props}
			className={twMerge("mx-auto w-full p-4", sizeClass, className)}
		/>
	)
})
