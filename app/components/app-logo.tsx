import { VenetianMask } from "lucide-react"

export function AppLogo({ small }: { small?: boolean }) {
	return <VenetianMask className={small ? "s-8" : "s-16"} aria-hidden />
}
