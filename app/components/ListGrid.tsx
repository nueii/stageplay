import { Fragment } from "react"
import type { EntryOf } from "~/helpers/types.ts"

export function ListGrid<
	Item extends Record<PropertyKey, React.Key>,
	ItemKey extends Extract<EntryOf<Item>, readonly [PropertyKey, React.Key]>[0],
>({
	items,
	itemKey,
	renderItem,
	emptyState,
}: {
	items: Item[]
	itemKey: ItemKey | ((item: Item) => React.Key)
	renderItem: (item: Item) => React.ReactNode
	emptyState: React.ReactNode
}) {
	return items.length === 0 ? (
		emptyState
	) : (
		<div className="grid gap-4 fluid-cols-56">
			{items.map((item) => (
				<Fragment
					key={itemKey instanceof Function ? itemKey(item) : item[itemKey]}
				>
					{renderItem(item)}
				</Fragment>
			))}
		</div>
	)
}
