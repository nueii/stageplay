import * as Popover from "@radix-ui/react-popover"
import { LucideCheck, VenetianMask } from "lucide-react"
import { matchSorter } from "match-sorter"
import { useState } from "react"
import { Virtuoso } from "react-virtuoso"
import { pageHeading } from "~/styles/common.ts"
import { Tooltip, TooltipContent, TooltipTrigger } from "./Tooltip.tsx"
import { Avatar } from "./avatar.tsx"
import { Button } from "./button.tsx"
import { Panel } from "./panel.tsx"

type CharacterItem = {
	id: string
	name: string
}

export function CharacterSwitcher({
	characters,
	currentCharacter,
	onSelectCharacter,
}: {
	characters: CharacterItem[]
	currentCharacter: CharacterItem
	onSelectCharacter: (character: CharacterItem) => void
}) {
	const [search, setSearch] = useState("")
	return (
		<Popover.Root>
			<Tooltip>
				<TooltipTrigger asChild>
					<Popover.Trigger
						className="p-2 hover:opacity-50 transition-opacity"
						// prevent showing tooltip on focus
						onFocus={(event) => event.preventDefault()}
					>
						<img
							src={`/characters/${currentCharacter.id}/avatar`}
							alt="Your current character's avatar"
							className="s-8 sm:s-12"
						/>
						<span className="sr-only">Switch character</span>
					</Popover.Trigger>
				</TooltipTrigger>
				<TooltipContent>Switch character</TooltipContent>
			</Tooltip>
			<Popover.Portal>
				<Panel
					border
					className="w-screen max-w-[320px] h-screen max-h-[480px] flex flex-col gap-3 p-4 bg-black radix-animation-fade-slide-down"
					asChild
				>
					<Popover.Content side="top" sideOffset={8} collisionPadding={8}>
						<h2 className={pageHeading("text-2xl/none")}>Switch character</h2>
						<input
							className="input"
							placeholder="Search..."
							value={search}
							onChange={(event) => {
								setSearch(event.target.value)
							}}
						/>
						<section className="flex-1">
							<h3 className="sr-only">Characters</h3>
							<Virtuoso
								className="focus-visible:ring-2 ring-accent-300 rounded-md border border-white/10"
								data={matchSorter(characters, search, {
									keys: ["name"],
								})}
								computeItemKey={(_index, character) => character.id}
								itemContent={(_index, character) => (
									<Popover.Close asChild>
										<Button
											className="w-full px-2 focus-visible:ring-2 ring-accent-300 ring-inset focus:outline-none"
											active={character.id === currentCharacter.id}
											onClick={() => onSelectCharacter(character)}
										>
											<Avatar
												src={`/characters/${character.id}/avatar`}
												alt=""
												size={24}
												fallbackIcon={VenetianMask}
											/>
											<div className="flex-1 text-left">{character.name}</div>
											{character.id === currentCharacter.id && (
												<LucideCheck className="s-5" />
											)}
										</Button>
									</Popover.Close>
								)}
							/>
						</section>
						<Button outline asChild>
							<Popover.Close>Close</Popover.Close>
						</Button>
					</Popover.Content>
				</Panel>
			</Popover.Portal>
		</Popover.Root>
	)
}
