import type { ReactNode } from "react"
import { container } from "~/styles/common.ts"
import { Panel } from "./panel.tsx"

export function CenteredPanelLayout({
	children,
	heading,
}: {
	children: ReactNode
	heading?: ReactNode
}) {
	return (
		<div className={container()}>
			<Panel border className="grid gap-4 p-4" asChild>
				<main>
					{heading ? (
						<h2 className="text-3xl font-light">{heading}</h2>
					) : undefined}
					{children}
				</main>
			</Panel>
		</div>
	)
}
