import type { Nullish } from "~/helpers/types.js"
import { errorText } from "~/styles/common.ts"

export function ErrorList({ errors }: { errors: Nullish<string[]> }) {
	return errors == null || errors.length === 0 ? null : (
		<ul className="grid gap-1">
			{[...new Set(errors)].map((error) => (
				<li key={error} role="alert" className={errorText()}>
					{error}
				</li>
			))}
		</ul>
	)
}
