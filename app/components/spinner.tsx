import type { ComponentPropsWithoutRef } from "react"

export function Spinner(props: ComponentPropsWithoutRef<"div">) {
	return (
		<div {...props}>
			<div className="grid animate-spin grid-cols-2 grid-rows-2 gap-[calc(100%/7)] p-[calc(100%/7)] s-full">
				<div className="rounded-full bg-accent-500 s-full" />
				<div className="rounded-full bg-accent-200 s-full" />
				<div className="rounded-full bg-accent-200 s-full" />
				<div className="rounded-full bg-accent-500 s-full" />
			</div>
		</div>
	)
}
