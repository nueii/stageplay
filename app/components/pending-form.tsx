import { Form, type FormProps, useNavigation } from "@remix-run/react"
import { type ComponentPropsWithoutRef, createContext, useContext } from "react"
import { useSpinDelay } from "spin-delay"
import { Spinner } from "./spinner"

const PendingFormContext = createContext(false)

export interface PendingFormProps extends Omit<FormProps, "children"> {
	children: React.ReactNode | ((props: { pending: boolean }) => React.ReactNode)
}

export function PendingForm({ children, ...props }: PendingFormProps) {
	const transition = useNavigation()

	const pending = useSpinDelay(
		transition.state !== "idle" &&
			transition.formMethod === props.method &&
			(props.action === undefined || transition.formAction === "/auth/discord"),
	)

	return (
		<Form {...props}>
			<PendingFormContext.Provider value={pending}>
				{typeof children === "function" ? children({ pending }) : children}
			</PendingFormContext.Provider>
		</Form>
	)
}

export interface PendingFormButtonProps
	extends ComponentPropsWithoutRef<"button"> {
	pending?: React.ReactNode
	type: "button" | "submit" | "reset"
}

export function PendingFormButton({
	children,
	pending: pendingChildren = (
		<>
			<Spinner className="button-icon" /> Please wait...
		</>
	),
	type,
	...props
}: PendingFormButtonProps) {
	const pending = useContext(PendingFormContext)
	return (
		<button {...props} type={type} disabled={pending || props.disabled}>
			{pending ? pendingChildren : children}
		</button>
	)
}
