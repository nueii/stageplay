import {
	DndContext,
	KeyboardSensor,
	MouseSensor,
	TouchSensor,
	closestCenter,
	useSensor,
	useSensors,
} from "@dnd-kit/core"
import {
	SortableContext,
	arrayMove,
	rectSortingStrategy,
	sortableKeyboardCoordinates,
	useSortable,
} from "@dnd-kit/sortable"
import { CSS } from "@dnd-kit/utilities"
import { ImagePlus, VenetianMask, X } from "lucide-react"
import { type ComponentPropsWithoutRef, useRef, useState } from "react"
import TextArea from "react-textarea-autosize"
import { z } from "zod"
import { zfd } from "zod-form-data"
import { FormField } from "~/components/form-field"
import { Avatar } from "./avatar"
import { BlurredBackgroundImage } from "./blurred-background-image"
import { PendingForm } from "./pending-form"

export const characterFormSchema = zfd.formData({
	name: zfd.text(z.string().min(1).max(100)),
	avatarUrl: zfd.text(z.string().optional()),
	images: zfd.repeatableOfType(z.string()),
	prefix: zfd.text(z.string().optional()),
	bio: zfd.text(z.string().max(10_000)),
})

export type CharacterFormData = z.output<typeof characterFormSchema>

export type CharacterFormErrors = {
	formErrors?: string[]
	// map over character form data for autocomplete/definition link, but still allow any other field errors
	fieldErrors?: Record<string, string[]> & {
		[_key in keyof CharacterFormData]?: string[]
	}
}

type ImageItem = {
	key: string
	url: string
}

const createImageItem = (url: string): ImageItem => ({
	url,
	key: String(Math.random()),
})

export function CharacterForm({
	character,
	errors,
	children,
}: {
	character?: Partial<CharacterFormData>
	errors: CharacterFormErrors | undefined
	children: React.ReactNode
}) {
	const [avatarUrl, setAvatarUrl] = useState(character?.avatarUrl)

	const [images, setImages] = useState<ImageItem[]>(
		character?.images?.map(createImageItem) ?? [],
	)

	return (
		<PendingForm method="post" className="grid gap-4">
			{errors?.formErrors && (
				<ul>
					{errors.formErrors.map((error, index) => (
						// biome-ignore lint/suspicious/noArrayIndexKey: <explanation>
						<li key={index} className="text-red-400">
							{error}
						</li>
					))}
				</ul>
			)}

			<FormField label="Name" errors={errors?.fieldErrors?.name}>
				<input
					className="input"
					type="text"
					name="name"
					placeholder="Hitori Gotou"
					required
					defaultValue={character?.name}
				/>
			</FormField>

			<FormField
				label="Prefix"
				description="Type this at the start of messages to play as this character."
				errors={errors?.fieldErrors?.prefix}
			>
				<input
					className="input"
					type="text"
					name="prefix"
					placeholder="hitori:"
					defaultValue={character?.prefix}
				/>
			</FormField>

			<div className="flex flex-col items-center gap-4 sm:flex-row">
				<div className="w-full shrink">
					<FormField
						label="Avatar URL"
						description="Shows next to each message you send"
						errors={errors?.fieldErrors?.avatarUrl}
					>
						<input
							className="input"
							type="url"
							name="avatarUrl"
							placeholder="https://example.com/avatar.png"
							value={avatarUrl}
							onChange={(event) => setAvatarUrl(event.target.value)}
						/>
					</FormField>
				</div>
				{!!avatarUrl?.trim() && (
					<Avatar
						size={96}
						src={avatarUrl.trim()}
						alt=""
						fallbackIcon={VenetianMask}
					/>
				)}
			</div>

			<FormField
				label="Images"
				description="Other reference images for your character"
				errors={errors?.fieldErrors?.images}
			>
				{(field) => (
					<div className="grid gap-2">
						<ImageInput
							{...field}
							onAdd={(url) =>
								setImages((images) => [createImageItem(url), ...images])
							}
						/>
						{images.map((image) => (
							<input
								type="hidden"
								name="images"
								value={image.url}
								key={image.key}
							/>
						))}
						<ImageGrid images={images} onChange={setImages} />
					</div>
				)}
			</FormField>

			<FormField
				label="Bio"
				description={
					<>
						Pronouns, orientation, height, likes, dislikes, and/or history. Or
						go wild and do your own thing!
						<br />
						<a
							href="https://commonmark.org/help/"
							target="_blank"
							rel="noreferrer"
							className="link"
						>
							Markdown
						</a>{" "}
						is supported.
					</>
				}
				errors={errors?.fieldErrors?.bio}
			>
				<TextArea
					className="input"
					name="bio"
					placeholder="Plays in an awesome indie J-rock band with other girls, all of whom are definitely straight."
					rows={3}
					defaultValue={character?.bio}
				/>
			</FormField>

			<div>{children}</div>
		</PendingForm>
	)
}

function ImageGrid(props: {
	images: ImageItem[]
	onChange: (items: ImageItem[]) => void
}) {
	const sensors = useSensors(
		useSensor(MouseSensor),
		useSensor(TouchSensor),
		useSensor(KeyboardSensor, {
			coordinateGetter: sortableKeyboardCoordinates,
		}),
	)

	return (
		<div className="grid gap-2 fluid-cols-48">
			<DndContext
				sensors={sensors}
				collisionDetection={closestCenter}
				onDragEnd={({ active, over }) => {
					if (over && active.id !== over.id) {
						const activeIndex = props.images.findIndex(
							(image) => image.key === active.id,
						)
						const overIndex = props.images.findIndex(
							(image) => image.key === over.id,
						)
						props.onChange(arrayMove(props.images, activeIndex, overIndex))
					}
				}}
			>
				<SortableContext
					items={props.images.map((image) => image.key)}
					strategy={rectSortingStrategy}
				>
					{props.images.map((image) => (
						<ImageGridItem
							key={image.key}
							image={image}
							onRemove={() => {
								props.onChange(props.images.filter((i) => i.key !== image.key))
							}}
						/>
					))}
				</SortableContext>
			</DndContext>
		</div>
	)
}

function ImageGridItem(props: { image: ImageItem; onRemove: () => void }) {
	const sortable = useSortable({ id: props.image.key })

	return (
		<div
			className="panel relative aspect-square"
			ref={sortable.setNodeRef}
			style={{
				transform: CSS.Translate.toString(sortable.transform),
				transition: sortable.transition,
			}}
		>
			<div
				{...sortable.attributes}
				{...sortable.listeners}
				className="absolute inset-0 cursor-grab data-[dragging]:cursor-grabbing"
				data-dragging={sortable.isDragging || undefined}
			>
				<BlurredBackgroundImage src={props.image.url} />
			</div>
			<button
				type="button"
				className="panel panel-interactive absolute right-2 top-2 p-1"
				title="Remove"
				onClick={props.onRemove}
			>
				<X className="button-icon" />
			</button>
		</div>
	)
}

function ImageInput({
	onAdd,
	...inputProps
}: ComponentPropsWithoutRef<"input"> & {
	onAdd: (value: string) => void
}) {
	const ref = useRef<HTMLInputElement>(null)

	const handleAdd = () => {
		if (!ref.current?.value.trim()) return
		onAdd(ref.current.value)
		ref.current.value = ""
	}

	return (
		<div className="flex gap-2">
			<input
				{...inputProps}
				className="input flex-1"
				type="url"
				placeholder="https://example.com/image.png"
				ref={ref}
				onKeyDown={(event) => {
					if (event.key === "Enter") {
						event.preventDefault()
						handleAdd()
					}
				}}
			/>
			<button
				type="button"
				className="button button-outline"
				onClick={handleAdd}
			>
				<ImagePlus className="button-icon" /> Add
			</button>
		</div>
	)
}
