import * as RadixTooltip from "@radix-ui/react-tooltip"
import { twMerge } from "tailwind-merge"
import { Panel } from "./panel.tsx"

export const TooltipProvider = RadixTooltip.Provider
export const Tooltip = RadixTooltip.Root
export const TooltipTrigger = RadixTooltip.Trigger

export function TooltipContent(props: RadixTooltip.TooltipContentProps) {
	return (
		<RadixTooltip.TooltipPortal>
			<Panel
				border
				className={twMerge(
					"text-sm/5 px-2 pt-1.5 pb-1 radix-animation-fade-zoom animate-duration-75 pointer-events-none",
					props.className,
				)}
				asChild
			>
				<RadixTooltip.TooltipContent {...props} />
			</Panel>
		</RadixTooltip.TooltipPortal>
	)
}
