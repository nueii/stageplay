import { ErrorLayout } from "~/components/error-layout"

export function NotFoundMessage() {
	return (
		<ErrorLayout title="Not found">
			<p>Couldn&apos;t find what you were looking for.</p>
		</ErrorLayout>
	)
}
