import { Link } from "@remix-run/react"
import type { Doc } from "convex-backend/_generated/dataModel.js"
import { formatDistanceToNow } from "date-fns"
import { useMemo } from "react"
import { renderMarkdown } from "~/helpers/markdown.ts"
import { errorText } from "~/styles/common.ts"
import styles from "./ChatMessage.module.css"

export interface ClientMessage extends Doc<"messages"> {
	pending?: true
	failed?: boolean
}

const dateFormat = new Intl.DateTimeFormat(undefined, {
	dateStyle: "medium",
	timeStyle: "medium",
})

export function ChatMessage({
	message,
	onRetrySubmit,
}: {
	message: ClientMessage
	onRetrySubmit: (message: ClientMessage) => void
}) {
	const { renderedText, isAction } = useMemo(() => {
		const actionCommandRegex = /^\s*\/me/
		const isAction = !!message.text.match(actionCommandRegex)
		const renderedText = renderMarkdown(
			message.text.replace(actionCommandRegex, ""),
		)
		return {
			renderedText,
			isAction,
		}
	}, [message.text])

	return (
		<div className="px-3 py-2 transition-colors hover:bg-white/5 flex items-start gap-2 group">
			<Link to={`/characters/${message.characterId}`}>
				<span className="sr-only">
					View profile for {message.characterName}
				</span>
				<img
					src={`/characters/${message.characterId}/avatar`}
					alt=""
					className="rounded-full s-8"
				/>
			</Link>

			<div className="flex-1 min-w-0 self-center">
				<time
					className="opacity-50 text-sm float-right group-hover:opacity-75 transition-opacity"
					dateTime={new Date(message._creationTime).toISOString()}
					title={dateFormat.format(message._creationTime)}
				>
					{formatDistanceToNow(message._creationTime, { addSuffix: true })}
				</time>
				<div
					data-pending={message.pending}
					data-action={isAction}
					className="data-[pending=true]:opacity-50 transition-opacity data-[action=true]:italic"
				>
					<Link
						to={`/characters/${message.characterId}`}
						data-action={isAction}
						className="mr-2 inline-block font-medium text-accent-300 transition-colors hover:text-accent-500 float-left data-[action=true]:before:content-['*']"
					>
						{message.characterName}
					</Link>
					<div
						className={`${styles.messageContent} ${
							message.failed ? errorText() : ""
						}`}
						// biome-ignore lint/security/noDangerouslySetInnerHtml: we're rendering markdown
						dangerouslySetInnerHTML={renderedText}
					/>
				</div>

				{message.failed && (
					<p role="alert" className="text-xs">
						Failed to send message.{" "}
						<button
							type="button"
							className="link"
							onClick={() => onRetrySubmit(message)}
						>
							Try again
						</button>
					</p>
				)}
			</div>
		</div>
	)
}
