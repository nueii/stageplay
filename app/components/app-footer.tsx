export function AppFooter() {
	return (
		<footer className="p-2 min-h-12 flex justify-center items-center text-center text-sm opacity-50 drop-shadow-md mt-auto">
			<p>
				background art by{" "}
				<a
					href="https://www.pixiv.net/en/artworks/104012265"
					target="_blank"
					rel="noreferrer"
					className="link"
					aria-label="Background art source"
				>
					karasuro
				</a>{" "}
				<span className="mx-1 inline-block" aria-hidden>
					&bull;
				</span>{" "}
				Stageplay is not affiliated with Discord or Discord Inc. in any way
			</p>
		</footer>
	)
}
