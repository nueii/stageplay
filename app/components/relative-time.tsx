import { formatDistanceToNow } from "date-fns"
import type { ComponentPropsWithoutRef } from "react"

export function RelativeTime({
	date,
	...props
}: Omit<ComponentPropsWithoutRef<"time">, "dateTime" | "children"> & {
	date: Date | string | number
}) {
	return (
		<time {...props} dateTime={new Date(date).toISOString()}>
			{formatDistanceToNow(new Date(date), { addSuffix: true })}
		</time>
	)
}
