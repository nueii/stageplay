import * as z from "zod"

const envResult = z
	.object({
		NODE_ENV: z
			.enum(["development", "production", "test"])
			.default("development"),
		COOKIE_SECRET: z.string(),
		DISCORD_CLIENT_ID: z.string(),
		DISCORD_CLIENT_SECRET: z.string(),
		DISCORD_REDIRECT_URI: z.string(),
		FLIST_ACCOUNT: z.string(),
		FLIST_PASSWORD: z.string(),
		CONVEX_URL: z.string(),
		CLERK_SECRET_KEY: z.string(),
		CLERK_ACCOUNT_PORTAL_URL: z.string(),
		PUSH_PUBLIC_KEY: z.string(),
	})
	.safeParse(process.env)

if (!envResult.success) {
	const missingVars = envResult.error.issues.map(
		(issue) => `- ${issue.path.join(".")}`,
	)
	throw new Error(`Missing environment variables:\n${missingVars.join("\n")}`)
}

export const env = envResult.data
