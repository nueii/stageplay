import { v } from "convex/values"
import { raise } from "~/helpers/common.ts"
import { mutation, query } from "./_generated/server.js"
import { requireIdentity } from "./auth.ts"

export const list = query({
	async handler(ctx) {
		const identity = await requireIdentity(ctx)

		const joinedRooms = await ctx.db
			.query("joinedRooms")
			.withIndex("by_user", (q) =>
				q.eq("userTokenIdentifier", identity.tokenIdentifier),
			)
			.collect()

		const rooms = await Promise.all(
			joinedRooms.map((joinedRoom) => ctx.db.get(joinedRoom.roomId)),
		)

		return rooms.filter(Boolean)
	},
})

export const create = mutation({
	args: {
		roomId: v.id("rooms"),
	},
	async handler(ctx, args) {
		const identity = await requireIdentity(ctx)

		const room = (await ctx.db.get(args.roomId)) ?? raise("Room not found")

		const existing = await ctx.db
			.query("joinedRooms")
			.withIndex("by_room_and_user", (q) =>
				q
					.eq("roomId", room._id)
					.eq("userTokenIdentifier", identity.tokenIdentifier),
			)
			.first()

		if (existing) {
			return
		}

		await ctx.db.insert("joinedRooms", {
			userTokenIdentifier: identity.tokenIdentifier,
			roomId: room._id,
		})
	},
})

export const remove = mutation({
	args: {
		roomId: v.id("rooms"),
	},
	async handler(ctx, args) {
		const identity = await requireIdentity(ctx)

		const existing = await ctx.db
			.query("joinedRooms")
			.withIndex("by_room_and_user", (q) =>
				q
					.eq("roomId", args.roomId)
					.eq("userTokenIdentifier", identity.tokenIdentifier),
			)
			.first()

		if (!existing) {
			return
		}

		await ctx.db.delete(existing._id)
	},
})
