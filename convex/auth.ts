import { raise } from "~/helpers/common.ts"
import type { QueryCtx } from "./_generated/server"

export async function requireIdentity(ctx: QueryCtx) {
	const identity = await ctx.auth.getUserIdentity()
	return identity ?? raise("Not logged in", requireIdentity)
}
