import { v } from "convex/values"
import { mutation, query } from "./_generated/server.js"
import { requireIdentity } from "./auth.ts"

export const get = query({
	args: {
		roomId: v.id("rooms"),
	},
	async handler(ctx, args) {
		const identity = await requireIdentity(ctx)

		const config = await ctx.db
			.query("roomConfigs")
			.withIndex("by_room_and_user", (q) =>
				q
					.eq("roomId", args.roomId)
					.eq("userTokenIdentifier", identity.tokenIdentifier),
			)
			.unique()

		return config
	},
})

export const update = mutation({
	args: {
		roomId: v.id("rooms"),
		character: v.optional(
			v.object({
				id: v.string(),
				name: v.string(),
			}),
		),
		message: v.optional(v.string()),
	},
	handler: async (ctx, args) => {
		const identity = await requireIdentity(ctx)

		const config = await ctx.db
			.query("roomConfigs")
			.withIndex("by_room_and_user", (q) =>
				q
					.eq("roomId", args.roomId)
					.eq("userTokenIdentifier", identity.tokenIdentifier),
			)
			.unique()

		if (config) {
			await ctx.db.patch(config._id, args)
		} else {
			await ctx.db.insert("roomConfigs", {
				...args,
				userTokenIdentifier: identity.tokenIdentifier,
			})
		}
	},
})
