import { v } from "convex/values"
import { raise } from "~/helpers/common.ts"
import { internal } from "./_generated/api.js"
import { mutation, query } from "./_generated/server.js"
import { requireIdentity } from "./auth.ts"
import { messagePayload } from "./schema.ts"

export const list = query({
	args: {
		roomId: v.id("rooms"),
	},
	async handler(ctx, args) {
		await requireIdentity(ctx)

		const messages = await ctx.db
			.query("messages")
			.withIndex("by_room", (q) => q.eq("roomId", args.roomId))
			.order("desc")
			.take(500)
		messages.reverse()

		return messages
	},
})

export const create = mutation({
	args: messagePayload,
	handler: async (ctx, args) => {
		const identity = await requireIdentity(ctx)
		const room = (await ctx.db.get(args.roomId)) ?? raise("Room not found")
		const text = args.text.trim() || raise("Message text cannot be empty")

		await ctx.db.insert("messages", {
			...args,
			text,
		})

		const memberRoomConfigs = await ctx.db
			.query("roomConfigs")
			.withIndex("by_room", (q) => q.eq("roomId", args.roomId))
			.collect()

		const subscriptions = await Promise.all(
			memberRoomConfigs
				.filter(
					(config) => config.userTokenIdentifier !== identity.tokenIdentifier,
				)
				.map((config) =>
					ctx.db
						.query("pushSubscriptions")
						.withIndex("by_user", (q) =>
							q.eq("userTokenIdentifier", config.userTokenIdentifier),
						)
						.first(),
				),
		)

		await ctx.scheduler.runAfter(0, internal.notifications.sendNotifications, {
			notifications: subscriptions.filter(Boolean).map((sub) => ({
				title: `${args.characterName} sent a message in ${room.title}`,
				options: {
					body: text,
					icon: `/characters/${args.characterId}/avatar`,
					data: {
						url: `/rooms/${args.roomId}`,
					},
				},
				subscription: {
					endpoint: sub.endpoint,
					keys: sub.keys,
				},
			})),
		})
	},
})
