import { defineSchema, defineTable } from "convex/server"
import { v } from "convex/values"

export const roomPayload = {
	title: v.string(),
}

export const messagePayload = {
	roomId: v.id("rooms"),
	characterId: v.string(),
	characterName: v.string(),
	text: v.string(),
}

export default defineSchema({
	rooms: defineTable({
		...roomPayload,
		ownerTokenIdentifier: v.string(),
	}).index("by_owner", ["ownerTokenIdentifier"]),

	messages: defineTable(messagePayload).index("by_room", ["roomId"]),

	pushSubscriptions: defineTable({
		userTokenIdentifier: v.string(),
		endpoint: v.string(),
		keys: v.object({
			p256dh: v.string(),
			auth: v.string(),
		}),
	}).index("by_user", ["userTokenIdentifier"]),

	roomConfigs: defineTable({
		roomId: v.id("rooms"),
		userTokenIdentifier: v.string(),
		character: v.optional(
			v.object({
				id: v.string(),
				name: v.string(),
			}),
		),
		message: v.optional(v.string()),
	})
		.index("by_user", ["userTokenIdentifier"])
		.index("by_room", ["roomId"])
		.index("by_room_and_user", ["roomId", "userTokenIdentifier"]),

	joinedRooms: defineTable({
		roomId: v.id("rooms"),
		userTokenIdentifier: v.string(),
	})
		.index("by_user", ["userTokenIdentifier"])
		.index("by_room_and_user", ["roomId", "userTokenIdentifier"]),
})
