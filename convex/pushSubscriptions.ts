import { v } from "convex/values"
import { mutation } from "./_generated/server.js"
import { requireIdentity } from "./auth.ts"

export const create = mutation({
	args: {
		endpoint: v.string(),
		keys: v.object({
			p256dh: v.string(),
			auth: v.string(),
		}),
	},
	async handler(ctx, args) {
		const identity = await requireIdentity(ctx)

		const [existing, ...extras] = await ctx.db
			.query("pushSubscriptions")
			.withIndex("by_user", (q) =>
				q.eq("userTokenIdentifier", identity.tokenIdentifier),
			)
			.collect()

		await Promise.all(extras.map((sub) => ctx.db.delete(sub._id)))

		if (existing) {
			await ctx.db.patch(existing._id, args)
		} else {
			await ctx.db.insert("pushSubscriptions", {
				...args,
				userTokenIdentifier: identity.tokenIdentifier,
			})
		}
	},
})
