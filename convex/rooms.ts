import { v } from "convex/values"
import { raise } from "~/helpers/common.ts"
import { mutation, query } from "./_generated/server.js"
import { requireIdentity } from "./auth.ts"

export const list = query({
	async handler(ctx, _args) {
		const identity = await requireIdentity(ctx)

		const ownedRooms = await ctx.db
			.query("rooms")
			.withIndex("by_owner", (q) =>
				q.eq("ownerTokenIdentifier", identity.tokenIdentifier),
			)
			.collect()

		const ownedRoomIds = new Set(ownedRooms.map((room) => room._id))

		const joinedRoomEntries = await ctx.db
			.query("joinedRooms")
			.withIndex("by_user", (q) =>
				q.eq("userTokenIdentifier", identity.tokenIdentifier),
			)
			.collect()

		const joinedRooms = await Promise.all(
			joinedRoomEntries
				.filter((entry) => !ownedRoomIds.has(entry.roomId))
				.map((entry) => ctx.db.get(entry.roomId)),
		)

		return {
			ownedRooms,
			joinedRooms: joinedRooms.filter(Boolean),
		}
	},
})

export const get = query({
	args: {
		roomId: v.id("rooms"),
	},
	async handler(ctx, args) {
		await requireIdentity(ctx)
		return await ctx.db.get(args.roomId)
	},
})

export const create = mutation({
	args: {
		title: v.string(),
	},
	handler: async (ctx, args) => {
		const identity = await requireIdentity(ctx)
		return ctx.db.insert("rooms", {
			...args,
			ownerTokenIdentifier: identity.tokenIdentifier,
		})
	},
})

export const remove = mutation({
	args: {
		roomId: v.id("rooms"),
	},
	handler: async (ctx, args) => {
		const identity = await requireIdentity(ctx)
		const room = (await ctx.db.get(args.roomId)) ?? raise("Room not found")

		if (room.ownerTokenIdentifier !== identity.tokenIdentifier) {
			raise("Failed to delete: user does not own the room")
		}

		await ctx.db.delete(args.roomId)
	},
})
