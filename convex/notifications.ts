"use node"

import { Validator, v } from "convex/values"
import { sendNotification } from "web-push"
import { raise } from "~/helpers/common.ts"
import { internalAction } from "./_generated/server.js"

export const sendNotifications = internalAction({
	args: {
		notifications: v.array(
			v.object({
				title: v.string(),
				options: v.object({
					body: v.string(),
					icon: v.optional(v.string()),
					data: v.optional(v.any() as Validator<unknown>),
				}),
				subscription: v.object({
					endpoint: v.string(),
					keys: v.object({
						p256dh: v.string(),
						auth: v.string(),
					}),
				}),
			}),
		),
	},

	async handler(_ctx, args) {
		const results = await Promise.all(
			args.notifications.map(async ({ subscription, ...notification }) => {
				try {
					await sendNotification(subscription, JSON.stringify(notification), {
						vapidDetails: {
							subject: "https://stageplay.nuei.dev",
							publicKey:
								process.env.PUSH_PUBLIC_KEY ?? raise("Missing push public key"),
							privateKey:
								process.env.PUSH_PRIVATE_KEY ??
								raise("Missing push private key"),
						},
					})
					return {
						success: true,
					} as const
				} catch (error) {
					return {
						success: false,
						error,
						notification,
					} as const
				}
			}),
		)

		for (const result of results) {
			if (!result.success) {
				console.error(
					"Sending notification failed.",
					result.error,
					result.notification,
				)
			}
		}

		if (results.some((r) => !r.success)) {
			throw new Error("Sending notifications failed. See logs for details.")
		}
	},
})
