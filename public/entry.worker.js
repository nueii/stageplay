/// <reference lib="webworker" />

const self = /** @type {ServiceWorkerGlobalScope} */ (
	/** @type {unknown} */ (globalThis.self)
)

self.addEventListener("install", (event) => {
	console.debug("Service worker installed")
	event.waitUntil(self.skipWaiting())
})

self.addEventListener("activate", (event) => {
	console.debug("Service worker activated")
	event.waitUntil(self.clients.claim())
})

self.addEventListener("push", (event) => {
	event.waitUntil(handlePush(event))
})

self.addEventListener("notificationclick", (event) => {
	event.notification.close()

	const { data } = event.notification
	const url = "url" in data && typeof data.url === "string" && data.url
	if (!url) return

	event.waitUntil(showPage(url))
})

/** @param {PushEvent} event */
async function handlePush(event) {
	/**
	 * @typedef {object} PushPayload
	 * @property {string} title
	 * @property {NotificationOptions} options
	 */

	try {
		/*&* @type {PushPayload} */
		const data = await event.data?.json()
		if (!data) return

		const notificationData = /** @type {unknown} */ data.options.data

		const url =
			typeof notificationData === "object" &&
			notificationData !== null &&
			"url" in notificationData &&
			typeof notificationData.url === "string" &&
			notificationData.url

		const windowClients = await self.clients.matchAll({ type: "window" })
		if (
			windowClients.some(
				(client) =>
					client.visibilityState === "visible" &&
					(!url || client.url.endsWith(url)),
			)
		) {
			return
		}

		await self.registration.showNotification(data.title, data.options)
	} catch (error) {
		console.error("Failed to handle push event - ", error, event)
	}
}

/** @param {string} url */
async function showPage(url) {
	try {
		const windowClients = await self.clients.matchAll({ type: "window" })

		const client =
			windowClients.find((windowClient) => windowClient.url.endsWith(url)) ??
			(await self.clients.openWindow(url))

		await client?.focus()
	} catch (error) {
		console.error(`Failed to show ${url} - `, error)
	}
}
