import { Router } from "express"
import { pathToRegexp } from "path-to-regexp"

const paths = [
	"/(.*).php",
	"/(.*).env",
	"/ads.txt",
	"/boaform(.*)",
	"/setup.cgi",
	"/cgi-bin(.*)",
	"/s3cmd.ini",
	"/HNAP1(.*)",
	"/php(.*)",
	"/db(.*)",
	"/sql(.*)",
	"/.git(.*)",
	"(.*)/wp-includes/(.*)",
	"/api(.*)",
	"/Autodiscover(.*)",
	"/_ignition(.*)",
	"/solr(.*)",
	"/wp-content(.*)",
	"/ReportServer(.*)",
	"/currentsetting(.*)",
	"/webfig(.*)",
	"/config(.*)",
	"/console(.*)",
]

let count = 0
export const botRedirects = paths.reduce((router, path) => {
	return router.all(pathToRegexp(path), (_req, res) => {
		res.redirect("https://www.youtube.com/watch?v=dQw4w9WgXcQ") // :)
		console.info("bots wrecked since start:", ++count)
	})
}, Router())
