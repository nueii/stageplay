import { createRequestHandler } from "@remix-run/express"
import { type ServerBuild, installGlobals } from "@remix-run/node"
import express, { Router } from "express"
import { runBot } from "../bot/main.tsx"
import { botRedirects } from "./bot-redirects.ts"

installGlobals()

async function createDevMiddleware() {
	const vite = await import("vite")

	const server = await vite.createServer({
		server: {
			middlewareMode: true,
		},
	})

	return Router()
		.use(server.middlewares)
		.use(express.static("build/client", { maxAge: "1h" }))
		.all(
			"*",
			createRequestHandler({
				build: async () => {
					const module = await server.ssrLoadModule(
						"virtual:remix/server-build",
					)
					return module as ServerBuild
				},
			}),
		)
}

async function createProdMiddleware() {
	// importing with a url so it doesn't get included in typechecks
	const build = await import(
		new URL("../build/server/index.js", import.meta.url).href
	)

	return Router()
		.use(
			"/assets",
			express.static("build/client/assets", {
				immutable: true,
				maxAge: "1y",
			}),
		)
		.use(express.static("build/client", { maxAge: "1h" }))
		.all(
			"*",
			createRequestHandler({
				build: build as object as ServerBuild,
			}),
		)
}

const app = express().use(botRedirects)

if (process.env.NODE_ENV === "production") {
	app.use(await createProdMiddleware())
} else {
	app.use(await createDevMiddleware())
}

const port = 3000
app.listen(port, () => {
	console.info(`Express server running on http://localhost:${port}`)
})

runBot().catch((error) => {
	console.error("Failed to run bot", error)
	process.exit(1)
})
