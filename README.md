# Stageplay

Be whoever you want! Create characters and send messages with their names and avatars. Simpler and modern alternative to Tupperbox

## Local development

1. [Install pnpm](https://pnpm.io/installation)
1. Install dependencies with `pnpm install`
1. Run `pnpm dev`
